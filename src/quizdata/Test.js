// QuizComponent.js
import React from 'react';
import './Quizcomponent.css'

const Test = () => {
  return (
    <div>
      <div className="start">
        <button>Start Quiz</button>
      </div>
      <div className="info_box">
        <div className="info_title">
          <span>Some rules of the Quiz</span>
        </div>
        <div className="info_list">
          <div className="info">1. You will have only <span>15 seconds</span> per each question</div>
          <div className="info">2. Once you select your answer you can't reselect</div>
          <div className="info">3. You can't select any option once the time goes off</div>
          <div className="info">4. You can't exit from the Quiz while you are playing</div>
          <div className="info">5. You'll get the points on the basis of your correct answers</div>
        </div>
        <div className="buttons">
          <button className="quit">Quit</button>&nbsp;
          <button className="restart">Continue</button>
        </div>
      </div>
      <div className="quiz-box">
        {/* Your quiz content goes here */}
      </div>
    </div>
  );
};

export default Test;
