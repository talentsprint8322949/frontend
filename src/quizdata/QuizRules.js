// QuizComponent.js
import React, { useState } from 'react';

const QuizRules = () => {
  const [quizStarted, setQuizStarted] = useState(false);

  const handleStartQuiz = () => {
    setQuizStarted(true);
    // Add any other logic needed when the quiz starts
  };

  const handleQuit = () => {
    // Add logic for quitting the quiz
    alert('Quiz Quit!'); // You can customize this action
  };

  const handleRestart = () => {
    // Add logic for restarting the quiz
    alert('Quiz Restarted!'); // You can customize this action
  };

  return (
    <div style={{alignItems:'center', textAlign:'center' , marginTop:'200px'}}>
      {quizStarted ? (
        <div className='info_box'>
          {/* Quiz content goes here */}
          <div>
            <span style={{color:'white',fontWeight:'300', fontSize:'15px'}}>Some rules of the Quiz</span>
          </div>
          <div className="info_list" style={{color:'white',fontWeight:'300', fontSize:'15px'}}>
            <div>1. You will have only 15 seconds per each question</div>
            <div>2. Once you select your answer you can't reselect</div>
            <div>3. You can't select any option once the time goes off</div>
            <div>4. You can't exit from the Quiz while you are playing</div>
            <div>5. You'll get the points on the basis of your correct answers</div>
          </div>
          <div>
            <button className="quit" style={{border:'none', borderRadius:'none', padding:'3px'}} onClick={handleQuit}>Quit</button>&nbsp;
            <button className="restart" style={{border:'none', borderRadius:'none', padding:'3px'}} onClick={handleRestart}>Continue</button>
          </div>
        </div>
      ) : (
        <div className="start">
          <button onClick={handleStartQuiz} style={{border:'none', borderRadius:'none', padding:'3px', fontSize:'25px', fontWeight:'400', color:'#007bff', background:'white', cursor:'pointer'}}>Start Quiz</button>
        </div>
      )}
    </div>
  );
};

export default QuizRules;
