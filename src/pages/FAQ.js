import React, { useState } from 'react';
import {
  Accordion,
  AccordionBody,
  AccordionHeader,
  AccordionItem,
} from 'reactstrap';

function FAQ(props) {
  const [open, setOpen] = useState('1');
  const toggle = (id) => {
    if (open === id) {
      setOpen();
    } else {
      setOpen(id);
    }
  };

  return (
    <div style={{alignItems:'flex-end'}}><br/>
      <Accordion open={open} toggle={toggle}>
        <AccordionItem>
          <AccordionHeader targetId="1">Are there any restrictions on Quiz Content?</AccordionHeader>
          <AccordionBody accordionId="1">
            <strong>Yes, there are couple of restrictions you need to know before starting the Quiz which were displaying in Home Page</strong>
           
          </AccordionBody>
        </AccordionItem>
        <AccordionItem>
          <AccordionHeader targetId="2">How do I find quizzes to take on Quizzify?</AccordionHeader>
          <AccordionBody accordionId="2">
            <strong>Upon logging into the Quizzify App it will redirect to homepage and there is a button Called <b>START QUIZ</b></strong>
            
          </AccordionBody>
        </AccordionItem>
        <AccordionItem>
          <AccordionHeader targetId="3">Is there a time limit for completing quizzes?</AccordionHeader>
          <AccordionBody accordionId="3">
            <strong>There is a time limit for every quiz and each quiz consists of 20 questions and timer for that is 30 minutes respectively.</strong>
            
          </AccordionBody>
        </AccordionItem>
        <AccordionItem>
          <AccordionHeader targetId="4">Can I save my progress and resume a quiz later?</AccordionHeader>
          <AccordionBody accordionId="4">
            <strong>No, you cannot pause quiz in the middle of attemption because we cannot control the timer</strong>
            
          </AccordionBody>
        </AccordionItem>
        <AccordionItem>
          <AccordionHeader targetId="5">How is my score calculated on Quizzify?</AccordionHeader>
          <AccordionBody accordionId="5">
            <strong>After succesful submission of quiz you will get the score based on the correct options you choose and completely based on weightage of each question.</strong>
            
          </AccordionBody>
        </AccordionItem>
        <AccordionItem>
          <AccordionHeader targetId="6">Can I review my quiz results after completing a quiz?</AccordionHeader>
          <AccordionBody accordionId="6">
            <strong>Yes, You can review your quiz results after submission of the quiz </strong>
           
          </AccordionBody>
        </AccordionItem>
        <AccordionItem>
          <AccordionHeader targetId="7">I'm experiencing technical issues with Quizzify. What should I do?</AccordionHeader>
          <AccordionBody accordionId="7">
            <strong>Kindly refresh your page and wait for some time to get the better internet connection to troubleshoot your internet issue.</strong>
            
          </AccordionBody>
        </AccordionItem>
        <AccordionItem>
          <AccordionHeader targetId="8">How do I change my password?</AccordionHeader>
          <AccordionBody accordionId="8">
            <strong>No, You cannot change your password.</strong>
            
          </AccordionBody>
        </AccordionItem>
        <AccordionItem>
          <AccordionHeader targetId="9">Why am I not receiving email notifications from Quizzify?</AccordionHeader>
          <AccordionBody accordionId="9">
            <strong>Kindly check in the spam box too.</strong>
            
          </AccordionBody>
        </AccordionItem>
       
      </Accordion>
    </div>
  );
}

export default FAQ;