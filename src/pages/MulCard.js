import React,{useState} from 'react';
import '../quizqns/Quiz.css'
// import '../pages/Mulcard.css'
// import '../pages/cards.css'
import { Container, Row, Col, Card, CardImg, CardBody, CardTitle, CardText, Button } from 'reactstrap';
import Quiz_css from '../quizqns/Quiz_css';
import Quiz from '../quizqns/Quiz';
import { Link } from 'react-router-dom';


const MulCard = () => {
  const [quizStarted, setQuizStarted] = useState(false);

  const handleStartClick = () => {
      setQuizStarted(true);
  };
  return (
    <React.Fragment>
      <Container className="text-center"  >
        <Row>
          <Col>
        
            <Card style={{ width: '18rem' }} className='trans'>
              <CardImg top src="https://www.citycot.com/wp-content/uploads/2021/02/htmllab.png"
style={{width:'287px',height:'200px'}} alt="Card image cap" />
              <CardBody>
                <CardTitle tag="h5">HTML</CardTitle>
             
              
                <Link to='/htmlquiz'><Button color="primary" className='buttonquiz' onClick={handleStartClick} style={{backgroundColor:'cornflowerblue'}}>Start Quiz</Button></Link>
              </CardBody>
            </Card>
           
          </Col>
          <Col>
            <Card style={{ width: '18rem' }}>
              <CardImg top src="https://icon-library.com/images/css-3-icon/css-3-icon-8.jpg" alt="Card image cap"style={{width:'287px',height:'200px'}}  />
              <CardBody>
                <CardTitle tag="h5">CSS</CardTitle>
               
                <Link to='/cssquiz' ><Button color="primary" className='buttonquiz' style={{backgroundColor:'cornflowerblue'}}>Start Quiz</Button> </Link>
                {/* <Button color="primary" className='buttonquiz' onClick={handleStartClick}>Start Quiz</Button> */}
              </CardBody>
            </Card>
          </Col>
          <Col>
         
            <Card style={{ width: '18rem' }}>
              <CardImg top src="https://assets-global.website-files.com/6009ec8cda7f305645c9d91b/62bdbdbea9169e08ba87e52b_What%20is%20Boostrap-Compressed.jpg" alt="Card image cap" style={{width:'287px',height:'200px'}}  />
              <CardBody>
                <CardTitle tag="h5">Bootstrap</CardTitle>
                
                <Link to='/bootquiz'><Button color="primary" className='button_quiz' style={{backgroundColor:'cornflowerblue'}}>Start Quiz</Button></Link>
              </CardBody>
            </Card>
      
          </Col>
        </Row><br/>
        <Row>
        <Col>
       
            <Card style={{ width: '18rem'}}>
              <CardImg top src="https://training.epam.com/static/news/508/WhatdoJavaScriptandasmartfridgehaveincommon_025255.png" alt="Card image cap" style={{width:'287px',height:'200px'}} />
              <CardBody>
                <CardTitle tag="h5">JavaScript</CardTitle>
                
                <Link to='/jsquiz'><Button color="primary" className='button_quiz' style={{backgroundColor:'cornflowerblue'}}>Start Quiz</Button> </Link>
                {/* <Button color="primary" className='buttonquiz'>Start Quiz</Button> */}
              </CardBody>
            </Card>
            
          </Col>
          <Col>
            <Card style={{ width: '18rem'}}>
              <CardImg top src="https://bs-uploads.toptal.io/blackfish-uploads/components/seo/content/og_image_file/og_image/786892/58-NodeJS__2x-cb33f70c56b0eb4df466056462ea3932.png" alt="Card image cap" style={{width:'287px',height:'200px'}}  />
              <CardBody>
                <CardTitle tag="h5">Node js</CardTitle>
                
                <Link to='/nodequiz'><Button color="primary" className='button_quiz' style={{backgroundColor:'cornflowerblue'}}>Start Quiz</Button></Link>
              </CardBody>
            </Card>
          </Col>
          <Col>
            <Card style={{ width: '18rem'}}>
              <CardImg top src="https://learn.microsoft.com/training/achievements/create-nodejs-project-dependencies-social.png" alt="Card image cap" style={{width:'287px',height:'200px'}} />
              <CardBody>
                <CardTitle tag="h5">Express js</CardTitle>
               
                <Link to='/expressquiz'><Button color="primary" className='button_quiz'style={{backgroundColor:'cornflowerblue'}}>Start Quiz</Button></Link>
              </CardBody>
            </Card>
          </Col>
        </Row><br/>
        <Row>
        <Col>
            <Card style={{ width: '18rem'}}>
              <CardImg top src="https://miro.medium.com/v2/resize:fit:1358/1*aSuUrHpObVqlmR1OA7FS3A.png" alt="Card image cap" style={{width:'287px',height:'200px'}} />
              <CardBody>
                <CardTitle tag="h5">Reactstrap</CardTitle>
               
                <Link to='/reactstrapquiz'><Button color="primary" className='button_quiz' style={{backgroundColor:'cornflowerblue'}}>Start Quiz</Button></Link>
              </CardBody>
            </Card>
          </Col>
          <Col>
            <Card style={{ width: '18rem'}}>
              <CardImg top src="https://miro.medium.com/v2/resize:fit:1200/1*4yHqe451juQRQeJ20kS0Ew.jpeg" alt="Card image cap" style={{width:'287px',height:'200px'}} />
              <CardBody>
                <CardTitle tag="h5">React</CardTitle>
                
                <Link to='/reactquiz'><Button color="primary" className='button_quiz' style={{backgroundColor:'cornflowerblue'}}>Start Quiz</Button></Link>
                
              </CardBody>
            </Card>
          </Col>
          <Col>
            <Card style={{ width: '18rem'}}>
              <CardImg top src="https://c4.wallpaperflare.com/wallpaper/284/730/367/computers-1920x1200-java-wallpaper-preview.jpg" alt="Card image cap" style={{width:'287px',height:'200px'}} />
              <CardBody>
                <CardTitle tag="h5">Java</CardTitle>
               
                <Link to='/javaquiz'><Button color="primary" className='button_quiz' style={{backgroundColor:'cornflowerblue'}}>Start Quiz</Button></Link>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
           
    </React.Fragment>
  );
};

export default MulCard;