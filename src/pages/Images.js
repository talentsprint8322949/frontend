import React from 'react';

const Images = () => {
  return (
    <div className='row' style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center', margin: '0 auto' }}>
      <div className='col-sm-2 col-md-4' style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
        <img src='https://assets-global.website-files.com/5e3ce2ec7f6e53c045fe7cfa/60c23b4e0f7a2574af209e13_Frame%2020.png' alt='' style={{ border: "1px solid black", borderRadius: "50%", height: "250px", width: "250px", border: "none", cursor: "pointer" }} />
        <h5>Unleash Your Knowledge, Embrace the Quiz.</h5>
      </div>
      <div className='col-sm-2 col-md-4' style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
        <img src='https://img.freepik.com/premium-vector/woman-with-light-bulb-creative-idea-creativity-find-solution-thinking-philosophy-innovation-puzzle-education-knowledge-vector-illustration-blue-colors-white-background_661108-3635.jpg?w=2000' alt='' style={{ border: "1px solid black", borderRadius: "50%", height: "250px", width: "250px", border: "none", cursor: "pointer" }} />
        <h5>Unlock Wisdom, Crack the Quiz Code.</h5>
      </div>
      <div className='col-sm-2 col-md-4' style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
        <img src='https://st4.depositphotos.com/1106647/38175/v/450/depositphotos_381756046-stock-illustration-cute-human-brain-jump-happy.jpg' alt='' style={{ border: "1px solid black", borderRadius: "50%", height: "245px", width: "245px", border: "none", cursor: "pointer" }} />
        <h5>Elevate Your Intelligence Instantly.</h5>
      </div>
    </div>
  );
};

export default Images;
