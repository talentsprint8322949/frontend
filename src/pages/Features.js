import React from 'react'
// import { Link } from 'react-router-dom';
import './Featurers.css'
const Features = () => {
  return (
    <section className="about-section">
      <div className="container">
        <div className="row">
          <div className="content-column col-lg-6 col-md-12 col-sm-12 order-2">
            <div className="inner-column">
              <div className="sec-title">
                {/* <span className="title">About Css3transition</span> */}
                <h2>About Quzzify</h2>
              </div>
              <div className="text" style={{fontSize:"20px" , textAlign:"justify",textJustify:"inter-word",color:'black'}}>
              We envision a world where learning is not just a task but a delightful experience. Quzzify is built on the idea that quizzes can be both educational and entertaining, encouraging users to explore new topics and expand their understanding of the world around them
              </div>
              <div className="text" style={{fontSize:"20px" , textAlign:"justify",textJustify:"inter-word",color:'black'}}>
              Diverse Categories: User-Friendly Interface: We've crafted an intuitive and user-friendly interface, making it easy for users of all ages to navigate and enjoy the quiz experience.

Educational Value: While our quizzes are designed to be entertaining, we also emphasize educational value. You'll not only have fun but also learn interesting facts and trivia along the way.

Community Building: Connect with like-minded individuals, challenge friends, and see how you stack up against players from around the world. Quzzify is not just an app; it's a community of knowledge seekers.
              </div>
              {/* <div className="btn-box">
                <a href="/Features" className="theme-btn btn-style-one">Contact Us</a>
              </div> */}
            </div>
          </div>

          {/* Image Column */}
          <div className="image-column col-lg-6 col-md-12 col-sm-12">
            <div className="inner-column wow fadeInLeft">
              <div className="author-desc">
                {/* <Link to = "/Home"><button style={{backgroundColor:"orange", border: "none", textDecoration:"none"}}>Quizzify</button></Link> */}
                <h2>Quizzify</h2>
                {/* <span>Web Developer</span> */}
              </div>
              <figure className="image-1">
                <a href="#ab" className="lightbox-image" data-fancybox="images">
                  <img title="Quizzify" src="https://img.freepik.com/premium-vector/quiz-blue-logo-isolate-white-questionnaire-icon-poll-sign-flat-bubble-speech-symbols_189959-1105.jpg" alt="" />
                </a>
              </figure>
            </div>
          </div>

        </div><br/><br/><br/>
        <div className="sec-title">
          {/* <span className="title">Our Future Goal</span> */}
          <h2>Quizzing Excellence - Where Smart Mind Gathers</h2>
        </div>
        <div className="text" style={{fontFamily: 'Times New Roman, Times, serif',fontSize:"20px" , textAlign:"justify",textJustify:"inter-word",color:'black'}}>
        Behind Quzzify is a dedicated team of developers, designers, and content creators who are passionate about creating an exceptional quiz experience. We're committed to regularly updating our content, enhancing features, and listening to user feedback to make Quzzify the best it can be.

        </div>
        <div className="text"style={{fontFamily: 'Times New Roman, Times, serif',fontSize:"20px" , textAlign:"justify",textJustify:"inter-word",color:'black'}}>
        We love hearing from our users! If you have any suggestions, feedback, or just want to say hello, feel free to reach out to us at [contact@Quizzify.com]. Your input is invaluable in shaping the future of Quzzify.

        </div>
        <div className="text"style={{fontFamily: 'Times New Roman, Times, serif',fontSize:"20px" , textAlign:"justify",textJustify:"inter-word",color:'black'}}>
        Thank you for being a part of the Quzzify community. Let the quizzing begin!
        </div>
      </div>
    </section>
  );
};

export default Features;
