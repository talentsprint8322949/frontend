import React from 'react';
import { Link } from 'react-router-dom';
import { Container, Row, Col, Card, CardImg, CardBody, CardTitle, CardText, Button } from 'reactstrap';

const Stucard = () => {
  return (
    <React.Fragment>
 
      
    <Container className="text-center" style={{backgroundImage:'url("https://images.ctfassets.net/co0pvta7hzrh/7alFZqXhj7fm9AJnuTwXNF/609868ff6e0aea0a96917ac03e31914b/shovels-spades-recommendation-quiz-deik17gm-thumbnail.png")',backgroundSize: "cover",
      backgroundRepeat: "no-repeat",width:'100vw',display:'flex',flexDirection:'row',alignItems:'center',justifyContent:'center'
      }}>
      <Row>
        <Col>
          <Card style={{ width: '18rem' ,border:'none', background:'none',marginRight:'130px'}}>
            <CardImg top src="https://static.vecteezy.com/system/resources/previews/000/357/104/original/vector-male-student-icon.jpg"
style={{width:'300px',height:'300px',borderRadius:'50%'}} alt="Card image cap" />
            <CardBody>
              <CardTitle tag="h5">STUDENT</CardTitle>
              <CardText>
                Student can start the exam here
              </CardText>
             <Link to='/login'> <Button color="primary" className='buttonquiz'>Enroll Now</Button></Link>
            </CardBody>
          </Card>
        </Col>
        <Col>
          <Card style={{ width: '18rem' ,border:'none', background:'none',marginLeft:'130px'
        }}>
            <CardImg top src="https://cdn-icons-png.flaticon.com/512/7079/7079591.png" alt="Card image cap"style={{width:'300px',height:'300px'}}  />
            <CardBody>
              <CardTitle tag="h5">ADMIN</CardTitle>
              <CardText>
                Admin can access from here
              </CardText>
             <Button color="primary" className='buttonquiz'>Access Here</Button>
            </CardBody>
          </Card>
        </Col>
        </Row>
        </Container>
  </React.Fragment>
  )
}

export default Stucard