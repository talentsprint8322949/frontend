// src/quizqns/Timer.js

import React, { useState, useEffect } from 'react';

const QuizTimer = (props) => {
  const quizDuration = 180; // 3 minutes
  const [timeRemaining, setTimeRemaining] = useState(quizDuration);
  const [timerInterval, setTimerInterval] = useState(null);
  const [message, setMessage] = useState('');

  useEffect(() => {
    const timeoutId = setTimeout(() => {
      setMessage('Delayed message after 2 seconds!');
    }, 2000);

    // Cleanup function to clear the timeout if the component unmounts
    return () => clearTimeout(timeoutId);
  })

  useEffect(() => {
    const localTimerInterval = setInterval(updateTimer, 1000);
    setTimerInterval(localTimerInterval);

    return () => clearInterval(localTimerInterval);
  }, []); 

  if(timeRemaining ===1){
    
    props.onTimerEnd();
  }
  // Function to update the timer
  const updateTimer = () => {
    setTimeRemaining((prevTime) => (prevTime > 0 ? prevTime - 1 : 0));
    
    // Check if the time has run out
    if (timeRemaining === 1) {
      
      // props.setLeft(false);
      clearInterval(timerInterval); // Stop the timer
       // You can customize this action
    }
   
    //  else {
    //   // Decrease the time remaining
      
    // }
  };

  return (
    <div>
      <h1>Timer</h1>
      <p style={{fontWeight:'bold'}}>Time: {timeRemaining} seconds</p>
    </div>
  );
};

export default QuizTimer;

