let express = require("express");
let mongodb = require("mongodb");
let mongoclient = mongodb.MongoClient;

let Signup = express.Router().post("/", function (req, res) {
  mongoclient.connect("mongodb://localhost:27017/QUIZZIFY", (err, db) => {
    if (err) {
      throw err;
    } else {
      db.collection("quizData").insertOne(
        {
          username: req.body.username,
          email: req.body.email,
          password: req.body.password,
          mobileNumber: req.body.mobileNumber,
        },
        (err, data) => {
          if (err) {
            throw err;
          } else {
            res.send(data);
          }
        }
      );
    }
  });
});

module.exports = Signup;