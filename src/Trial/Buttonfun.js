// ButtonFunctionalityComponent.js
import React, { useEffect } from 'react';

const Buttonfun = () => {
  useEffect(() => {
    // Import your external JavaScript file
    import('../quizqns/Combined.js')
      .then((module) => {
        // Access the module and execute any initialization logic if needed
        module.initializeFunction();
      })
      .catch((error) => {
        console.error('Error loading external JavaScript file:', error);
      });

    // Cleanup any resources if needed when the component unmounts
    return () => {
      // Cleanup logic, if necessary
    };
  }, []); // Empty dependency array means this effect runs once after the initial render

  return (
    <div>
      <button id="myButton">Click me</button>
    </div>
  );
};

export default Buttonfun;
