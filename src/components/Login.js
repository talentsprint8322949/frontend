import React, { useEffect, useState } from 'react';
import './Login.css'
import { useNavigate } from 'react-router-dom';
import axios from 'axios'
const Login = () => {

    const navigate = useNavigate ;
    useEffect(() => {
      const auth = localStorage.getItem('token')
      if(auth){
        navigate('/Mulcards')
      }
    }) 
    const[Username,setUsername] = useState('');
    // const[data,setData] = useState({Username:'',mail:'',Password:'',})
    const[mail,setmail] = useState('');
    const[Password,setPassword] = useState('');
    const handleLogin = async () =>{
        try{const response = await axios.post("https://backend-ozlj.onrender.com/login",{
            Username,
            Password
        })
        console.log(response.data);
        if(response.data.Username === Username ){
            localStorage.setItem('token','loggedin')
            alert("Login Succesful")
            window.location.href='/Mulcards'
        }else{
            alert("Login Failed")
        }
        }catch(error){
            alert("An error occured during login.")
        }
    }
    const handleregister = async () => { 
        try{
            const response = await  axios.post("https://backend-ozlj.onrender.com/signup",{
              Username,
            mail,
            Password
            });
            if(response.status === 200){
                alert("SignUp Succesful")
            }
            else{
                alert("SignUp failed")
            }
        }catch(error){
            alert("signUp Failed")
        }    
    };
  const handleSignup = () => {
    document.querySelector(".login-form-container").style.cssText = "display: none;";
    document.querySelector(".signup-form-container").style.cssText = "display: block;";
    document.querySelector(".login_container").style.cssText = "background: linear-gradient(to bottom, rgb(56, 189, 149),  rgb(28, 139, 106));";
    document.querySelector(".button-1").style.cssText = "display: none";
    document.querySelector(".button-2").style.cssText = "display: block";
  };

  const handlelogin = () => {
    document.querySelector(".signup-form-container").style.cssText = "display: none;";
    document.querySelector(".login-form-container").style.cssText = "display: block;";
    document.querySelector(".login_container").style.cssText = "background: linear-gradient(to bottom, rgb(6, 108, 224),  rgb(14, 48, 122));";
    document.querySelector(".button-2").style.cssText = "display: none";
    document.querySelector(".button-1").style.cssText = "display: block";
  };

  return (
    <div className="login_container">
      {/* Data or Content */}
      <div className="box-1">
        <div className="content-holder">
          <h2>Hello!</h2>
          <button className="button-1" onClick={handleSignup}>Sign up</button>
          <button className="button-2" onClick={handlelogin}>Sign in</button>
        </div>
      </div>
      {/* Forms */}
      <div className="box-2">
        {/* Login Form */}
        <div className="login-form-container">
          <h1>Sign in</h1>
          <input type="text" placeholder="Username" className="input-field" onChange={(e) => setUsername(e.target.value)}/>
          <br /><br />
          <input type="password" placeholder="Password" className="input-field" onChange={(e) => setPassword(e.target.value)}/>
          <br /><br />
          <button className="login-button" type="button" onClick={handleLogin}>Sign in</button>
        </div>
        {/* Signup Form */}
        <div className="signup-form-container">
          <h1>Sign up </h1>
          <input type="text" placeholder="Username" className="input-field"  onChange={(e) => setUsername(e.target.value)} />
          <br /><br />
          <input type="email" placeholder="Email" className="input-field" onChange={(e) => setmail(e.target.value)}/>
          <br /><br />
          <input type="password" placeholder="Password" className="input-field" onChange={(e) => setPassword(e.target.value)}/>
          <br /><br />
          <button className="signup-button" type="button" onClick={handleregister}>Sign up</button>
        </div>
      </div>
    </div>
  );
};

export default Login;