import React, { useState,useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import {  FormGroup } from 'reactstrap';
import axios from 'axios'; // Import axios for making HTTP requests
import './User.css'
import Footerquiz from './Footerquiz';

const User = () => {

  const [data, setData] = useState({ Username: '', Password: '' });
  const [errors, setErrors] = useState({ Username: '', Password: '' });
  const navigate = useNavigate
  useEffect(() => {
    const auth = localStorage.getItem('token');
    if (auth) {
      navigate('/home');
    }
  }, [navigate]);

  const validateLogin = () => {
    const errors = {};
    if (!data.Username) {
      errors.Username = 'Username is required';
    }
    if (!data.Password) {
      errors.Password = 'Password is required';
    }
    setErrors(errors);
    return Object.keys(errors).length === 0;
  };

  const loginUser = async (e) => {
    e.preventDefault();
    if (validateLogin()) {
      try {
        const response = await axios.post('http://localhost:7700/login', data); // Update the URL to your login endpoint
        if (response.status === 200) {
          localStorage.setItem('token', 'loggedin');
          // Redirect to the homepage or any other route upon successful login
        //   window.location.href = '/home';
        alert("Login Sucessful")
        } else {
          alert('Login Failed, please try again');
        }
      } catch (error) {
        console.error(error);
        alert('please try again');
      }
    } else {
      alert('Please check the form for errors.');
    }
  };



  return (
    <div >

    <div className='register '>
  <div className="logincont" style={{backgroundColor:"#2c2c2c" ,padding:"2% 2%"}} >  
  <h5 className="text-center mb-2 mt-2" style={{ color: 'white', fontWeight: 'bold' }}>Login</h5>
  <form onSubmit={loginUser}>
    <FormGroup className='mb-3'>
     
        <label htmlFor="Username" style={{ color: 'white', padding: '2px 4px' }}>Username</label>
        <input
          type="text"
          placeholder="Enter your Username"
          value={data.Username}
          onChange={(e) => setData({ ...data, Username: e.target.value })}
          className="form-control rounded-2"
        />
        {errors.Username && <div style={{ color: 'red' }}>{errors.Username}</div>}
    </FormGroup>
    <FormGroup className='mb-3'>
      <div className="mb-3">
        <label htmlFor="password" style={{ color: 'white', padding: '2px 4px' }}>Password</label>
        <input
          type="password"
          placeholder="Enter your password"
          value={data.Password}
          onChange={(e) => setData({ ...data, Password: e.target.value })}
          className="form-control rounded-2"
        />
        {errors.Password && <div style={{ color: 'red' }}>{errors.Password}</div>}
      </div>
    </FormGroup>
    <button type="submit" style={{ backgroundColor: '#FF0000', color: '#fff'}} className=" w-100 rounded-0 mx-auto 16px 0px 0px btn ">
      Login
    </button>
    <p className="text-light mt-2">
      Don't have an account? <Link to="/register" style={{ color: '#FF0000' }}>Register here.</Link>
    </p>
  </form>
</div>

    </div>
    <Footerquiz />
    </div>
    

  );
};

export default User;
