import React from 'react'
import './Footer.css'
import 'bootstrap/dist/css/bootstrap.min.css';

const Footerquiz = () => {
    return (
        <>
            <div className="Footer" >
                <div className="footer_container">
                    <div className="row"  style={{display:"flex",justifyContent:"space-around"}}>
                        <div className="col-md-6 col-lg-5 col-12 ft-1">
                            <h3><span>Quiz</span>zify</h3>
                            <p>Quizzify provides a health literacy promotion program, which engages employees in a game using short, multiple-choice and true-false quizzes</p>
                            <div className="footer-icons">
                                <i class="fa-brands fa-facebook"></i>
                                <i class="fa-brands fa-twitter"></i>
                                <i class="fa-brands fa-instagram"></i>
                                <i class="fa-brands fa-linkedin-in"></i>
                            </div>
                        </div>
                        {/* <div className="col-md-6 col-lg-3 col-12 ft-2">
                            <h5>Quick Links</h5>
                            <ul>
                                <li className="nav_item">
                                    <a className="a" href="/">HTML</a>
                                </li>
                                <li className="nav_item">
                                    <a className="a" href="/">CSS</a>
                                </li>
                                <li className="nav_item">
                                    <a className="a" href="/">JAVASCRIPT</a>
                                </li>
                                <li className="nav_item">
                                    <a className="a" href="/">REACT</a>
                                </li>
                                <li className="nav_item">
                                    <a className="a" href="/">JAVA</a>
                                </li>
                            </ul>
                        </div> */}
                        <div className="col-md-6 col-lg-4 col-12 ft-3">
                            <h5>Contact Us</h5>
                            <p><i class="fa-solid fa-phone-volume"></i> +91 9876543210</p>
                            <p><i class="fa-solid fa-envelope"></i> Quizzify@gmail.com</p>
                            <p><i class="fa-solid fa-paper-plane"></i> Hyderabad,Telangana,India</p>
                        </div>
                    </div>
                </div>
            </div>
            <div className='Last-footer'>
                <p>@Copyright2024</p>
            </div>
        </>
    )
}

export default Footerquiz;
