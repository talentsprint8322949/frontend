import React, { useState } from "react";
import axios from "axios";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input,
  Row,
  Col,
  ModalFooter,
} from "reactstrap";

function Modalregister(props) {
  const [modal, setModal] = useState(false);
  const [formdata, setFormdata] = useState({
    username: "",
    email: "",
    password: "",
    mobileNumber: "",
  });

  const handleInput = (e) => {
    const { name, value } = e.target;
    setFormdata({
      ...formdata,
      [name]: value,
    });
  };

  console.log(formdata);

  const toggle = () => {
    setModal(!modal);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      let response = await axios.post("http://localhost:3002/insert", formdata);
      console.log(response);
      alert("Registered Successfully");
    } catch (err) {
      throw err;
    }
  };

  return (
    <div>
      <Modal isOpen={props.isOpen} toggle={props.toggle}>
        <ModalHeader toggle={props.toggle}>Registration</ModalHeader>
        <ModalBody>
          <Form onSubmit={handleSubmit}>
            <Row>
              <Col md={6}>
                <FormGroup>
                  <Label for="username">Username</Label>
                  <Input
                    id="username"
                    name="username"
                    placeholder="Username"
                    type="text"
                    value={formdata.username}
                    onChange={handleInput}
                    required
                  />
                </FormGroup>
              </Col>
              <Col md={6}>
                <FormGroup>
                  <Label for="examplePassword">Password</Label>
                  <Input
                    id="examplePassword"
                    name="password"
                    placeholder="password "
                    type="password"
                    // pattern="(?=.\d)(?=.[a-z])(?=.*[A-Z]).{8,}"
                    value={formdata.password}
                    onChange={handleInput}
                    required
                  />
                </FormGroup>
              </Col>
            </Row>
            <FormGroup>
              <Label for="email">Email</Label>
              <Input
                id="email"
                name="email"
                placeholder="Email"
                type="email"
                value={formdata.email}
                onChange={handleInput}
                required
              />
            </FormGroup>
            <FormGroup>
              <Label for="mobileNumber">Mobile Number</Label>
              <Input
                id="mobileNumber"
                name="mobileNumber"
                placeholder="Mobile Number"
                type="tel"
                value={formdata.mobileNumber}
                onChange={handleInput}
                required
              />
            </FormGroup>
            <FormGroup check>
              <Input id="exampleCheck" name="check" type="checkbox" />
              <Label check for="exampleCheck">
                Check me out
              </Label>
            </FormGroup>
            <Button type="submit">Register</Button>
          </Form>
        </ModalBody>
      </Modal>
    </div>
  );
}

export default Modalregister;