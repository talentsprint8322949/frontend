import React, { createContext, useContext, useState } from "react";

const UserContext = createContext();

export const UserProvider = ({ children }) => {
  const [loggedInUserEmail, setLoggedInUserEmail] = useState(null);

  const loginUser = (email) => {
    setLoggedInUserEmail(email);
  };

  const logoutUser = () => {
    setLoggedInUserEmail(null);
  };

  return (
    <UserContext.Provider value={{ loggedInUserEmail, loginUser, logoutUser }}>
      {children}
    </UserContext.Provider>
  );
};

export const useUser = () => {
  return useContext(UserContext);
};