import React, { useState} from 'react';
import {useNavigate } from 'react-router-dom';
// import axios from 'axios';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
} from 'reactstrap';

function NavBar(args) {
  const [isOpen, setIsOpen] = useState(false);
  const navigate = useNavigate("")
  const auth = localStorage.getItem('token');

  const logout = () => {
    localStorage.removeItem('token');
    navigate('/');
  };

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div >
      <Navbar {...args}  light expand="md" style={{backgroundColor:'cornflowerblue'}} >
        <NavbarBrand href="/"><h5 style={{
    fontWeight: "700",
    fontFamily: "cursive",
    letterSpacing: "2px",
    textDecoration:'none'
    // padding: "0.5rem 0"
  }}><span style={{color:'white'}}>Quiz</span>zify</h5></NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="me-auto" navbar>
            {auth ? (
              <>
                <NavItem>
                  <NavLink href="/" style={{color:'white'}}>Home</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href="/about" style={{color:'white'}}>Features</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href="/help" style={{color:'white'}}>FAQ</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink onClick={logout} style={{cursor:"pointer",color:'white'}}>Logout</NavLink>
                </NavItem>
              </>
            ) : (
              <>
              <NavItem>
                  <NavLink href="/" style={{color:'white'}}>Home</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href="/about" style={{color:'white'}}>Features</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href="/help" style={{color:'white'}}>FAQ</NavLink>
                </NavItem>
                </>

            ) }
          </Nav>
        </Collapse>
      </Navbar>
    </div>
  );
}

export default NavBar;