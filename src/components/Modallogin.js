import React, { useState } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";
// import './Login.css';
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";
import { useUser } from "./UserContext";



const Modallogin = ({ isOpen, toggle, toggleRegisterModal }) => {
  const navigate = useNavigate();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [loginError, setLoginError] = useState(null);
  

  const handleLogin = async (e) => {
    e.preventDefault();
    try {
      let response = await axios.get(
        `http://localhost:7700/login`
      );

      if (response.data) {
        console.log("Login successful");
        alert(`Hi ${response.data.username}`);
        toggle();

        // Update user email in the context
        // loginUser(email);

        // Redirect to the home page
        navigate(`/`);
      } else {
        console.log("Login failed");
        alert("Your credentials are incorrect");
        toggle();
      }
    } catch (err) {
      console.error(
        "Login error:",
        err.response.data.error || "An error occurred during login"
      );
      if (err.response && err.response.data) {
        setLoginError(
          err.response.data.error || "An error occurred during login"
        );
      } else {
        setLoginError("An error occurred during login");
      }
    }
  };

  return (
    <div>
      <Modal isOpen={isOpen} toggle={toggle}>
        <ModalHeader toggle={toggle}>Login</ModalHeader>
        <ModalBody>
          <Form onSubmit={handleLogin}>
            <FormGroup>
              <Label for="email">Email</Label>
              <Input
                type="text"
                name="email"
                id="email"
                placeholder="Enter your email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Label for="password">Password</Label>
              <Input
                type="password"
                name="password"
                id="password"
                placeholder="Enter your password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </FormGroup>
            {loginError && <p style={{ color: "red" }}>{loginError}</p>}
            <Button color="primary" type="submit">
              Login
            </Button>{" "}
            <Link
              to="#"
              onClick={toggleRegisterModal}
              style={{
                textDecoration: "none",
                color: "Black",
                paddingLeft: "20px",
                paddingRight: "20px",
              }}
            >
              Not registered? Register here.
            </Link>
            <Button color="secondary" onClick={toggle}>
              Cancel
            </Button>
          </Form>
        </ModalBody>
      </Modal>
    </div>
  );
};

export default Modallogin;