import './App.css';
import MulCard from './pages/MulCard';
import Features from './pages/Features';
import Navbaaar from './components/Navbaaar';
import Home from './pages/Home';
import { BrowserRouter,Routes,Route } from 'react-router-dom';
import Quiz from './quizqns/Quiz';
import Quiz_react from './quizqns/Quiz_react'
import Quiz_node from './quizqns/Quiz_node';
import Footerquiz from './components/Footerquiz';
// import Viewpage from './components/Viewpage';
import Quiz_css from './quizqns/Quiz_css';
import Quiz_html from './quizqns/Quiz_html';
import Quiz_boot from './quizqns/Quiz_boot';
import Quiz_express from './quizqns/Quiz_express';
import Quiz_java from './quizqns/Quiz_java';
import Quiz_reactstrap from './quizqns/Quiz_reactstrap';
import FAQ from './pages/FAQ';
import Login from './components/Login';
// import Admin from './Admin/Admin';




function App() {
  return (
    // 
    <div>
    
      <BrowserRouter>
     
      <Navbaaar/>
      <Routes>
    <Route path='login' element={<Login/>}/>
    <Route path='/cssquiz' element={<Quiz_css/>}/>
    <Route path='/htmlquiz' element={<Quiz_html/>}/>
    <Route path='/bootquiz' element={<Quiz_boot/>}/>
    <Route path='/expressquiz' element={<Quiz_express/>}/>
    <Route path='/jsquiz' element={<Quiz/>}/>
    <Route path='/Mulcards' element={<MulCard/>}/>
    <Route path='/javaquiz' element={<Quiz_java/>}/>
    <Route path='/reactstrapquiz' element={<Quiz_reactstrap/>}/>
    <Route path='/reactquiz' element={<Quiz_react/>}/>
    <Route path='/nodequiz' element={<Quiz_node/>}/>
        <Route path='/' element={<Home/>}/>
        <Route path='/about' element={<Features/>}/>
        <Route path='/help' element={<FAQ/>}/>

      </Routes>
      <Footerquiz/>
      </BrowserRouter>
{/* <Admin/> */}













      {/* <Quiz/> */}
      {/* <Quiz_css/> */}
      {/* <Quiz_react/> */}
      {/* <Quiz_node/> */}
       {/* <Test/> */}
      {/*<QuizRules/> */}
      {/* <Combined/> */}
     
    </div>
  );
}

export default App;
