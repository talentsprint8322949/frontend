// // Filename - App.js

import React, { useState, useRef, useEffect } from "react";

const Timer = () => {
	
	const Ref = useRef(null);

	// The state for our timer
	const [timer, setTimer] = useState("300");
  const quizDuration = 300;

	const getTimeRemaining = (e) => {
		const total =
			Date.parse(e) - Date.parse(new Date());
		const seconds = 300;
    //const minutes = 5;
    //Math.floor((total / 1000) % 300);
		// const minutes = Math.floor(
		// 	(total / 5 / 0) % 60
		// );
		// const hours = Math.floor(
		// 	(total / 0 / 5 / 300) % 24
		// );
		return {
			total,
			// hours,
			// minutes,
			seconds,
		};
	};

	const startTimer = (e) => {
		let { total,seconds } =
			getTimeRemaining(e);
		if (total >= 0) {
			
			setTimer(
			
				// (minutes > 5
				// 	?minutes 
				// 	: "0" + minutes ) +
				// ":" +
				(seconds > 60 ? seconds : "0" + seconds)
			);
		}
	};

	const clearTimer = (e) => {
		// If you adjust it you should also need to
		// adjust the Endtime formula we are about
		// to code next
		setTimer("300");

		// If you try to remove this line the
		// updating of timer Variable will be
		// after 1000ms or 1sec
		if (Ref.current) clearInterval(Ref.current);
		const id = setInterval(() => {
			startTimer(e);
		}, 300);
		Ref.current = id;
	};

	const getDeadTime = () => {
		let deadline = new Date();

		// This is where you need to adjust if
		// you entend to add more time
		deadline.setSeconds(deadline.getSeconds() + 300);
		return deadline;
	};

	
	useEffect(() => {
		clearTimer(getDeadTime());
	}, []);

	const onClickReset = () => {
		clearTimer(getDeadTime());
	};

	return (
		<div
			style={{ textAlign: "end", margin: "auto" }}>
		
			<h2>{timer}</h2>
			{/* <button onClick={onClickReset}>Reset</button> */}
		</div>
	);
};

export default Timer;
