export const data =
[{
  question: "What is Reactstrap?",
  option1: "A testing library for React",
  option2: "A library that provides Bootstrap components as React components",
  option3: "A state management library for React",
  option4: "All of these",
  ans: 2
},
{
  question: "How do you install Reactstrap in a React project?",
  option1: "npm install react-bootstrap",
  option2: "npm install reactstrap",
  option3: "npm install bootstrap",
  option4: "npm i bootstrap",
  ans: 2
},
{
  question: "What is the role of Bootstrap in conjunction with Reactstrap?",
  option1: "Bootstrap is not required for Reactstrap",
  option2: "Bootstrap provides additional styling for React components",
  option3: "Bootstrap is the underlying CSS framework Reactstrap leverages",
  option4: "None of these",
  ans: 3
},
{
  question: "How do you use a Reactstrap component in a React application?",
  option1: "Import the desired Reactstrap component and use it in JSX syntax within the component's render method.",
  option2: "Install the component using npm install and then include it in the HTML file.",
  option3: "Use the create-react-component command to generate Reactstrap components.",
  option4: "All of these",
  ans: 1
},
{
  question: "Explain the purpose of the <Container> component in Reactstrap.",
  option1: "Creates a responsive container for other Reactstrap components.",
  option2: "Defines a navigation bar in Reactstrap.",
  option3: "Provides a layout for modal components.",
  option4: "All of these",
  ans: 1
},
{
  question: "What is the significance of the <Row> component in Reactstrap layouts?",
  option1: "Defines a row within a Reactstrap layout.",
  option2: "Represents a table row in React applications.",
  option3: "Creates a responsive navigation bar.",
  option4: "None of these",
  ans: 1
},
{
  question: "How can you integrate custom styles with Reactstrap components?",
  option1: "Use the style prop or add custom CSS classes.",
  option2: "Import external style sheets directly.",
  option3: "Apply styles using the class prop.",
  option4: "None of these",
  ans: 1
},
{
  question: "Explain the role of the <Col> component in Reactstrap layouts.",
  option1: "Defines a column within a Reactstrap layout.",
  option2: "Represents a collapsible component.",
  option3: "Provides a container for form controls.",
  option4: "Both (A) and (B)",
  ans: 1
},
{
  question: "How do you handle responsive design with Reactstrap components?",
  option1: "Use Bootstrap's responsive utility classes.",
  option2: "Use custom media queries.",
  option3: " Responsive design is not supported in Reactstrap.",
  option4: "Both (A) and (B)",
  ans: 1
},
{
  question: "What is the purpose of the <Navbar> component in Reactstrap?",
  option1: "Defines a row within a Reactstrap layout.",
  option2: "Represents a modal in React applications.",
  option3: "Both (A) and (B)",
  option4: "Creates a responsive navigation bar with various components.",
  ans: 4
},
{
  question: "How can you incorporate icons into Reactstrap components?",
  option1: "Use the icon prop available in Reactstrap components.",
  option2: "Integrate third-party icon libraries like Font Awesome.",
  option3: "Both (A) and (B)",
  option4: "Add images with the src attribute.",
  ans: 2
},
{
  question: "Explain the significance of the <Button> component in Reactstrap.",
  option1: "Defines a column within a Reactstrap layout.",
  option2: "Represents a collapsible component",
  option3: "None of these",
  option4: " Used for creating interactive buttons.",
  ans: 4
},
{
  question: "How do you create a modal using Reactstrap?",
  option1: "Use the <Modal> component and toggle it using the modal prop.",
  option2: "Use the <Dialog> component and manage its state.",
  option3: "None of these",
  option4: "Create a custom modal component from scratch.",
  ans: 1
},
{
  question: "What is the purpose of the <Card> component in Reactstrap?",
  option1: "Defines a column within a Reactstrap layout",
  option2: "Represents a flexible and extensible container for content.",
  option3: "Creates a responsive navigation bar.",
  option4: "All of these",
  ans: 2
},
{
  question: "How can you make a Reactstrap component disabled?",
  option1: "Set the disabled prop to true.",
  option2: "Use the enable prop.",
  option3: "Add the disabled CSS class.",
  option4: "None of these",
  ans: 1
},
{
  question: "Explain the difference between the <InputGroup> and <InputGroupAddon> components.",
  option1: "There is no difference between them; they are interchangeable.",
  option2: "<InputGroup> is used for navigation, while <InputGroupAddon> is used for buttons.",
  option3: "<InputGroup> is used to group multiple form controls, while <InputGroupAddon> is used to add additional content.",
  option4: "None of these",
  ans: 3
},
{
  question: "How do you handle form submissions in Reactstrap?",
  option1: "Use the submit prop in the form.",
  option2: "Use the <Form> and <FormGroup> components to structure the form and handle submissions using the onSubmit event.",
  option3: "Form submissions are handled automatically in Reactstrap.",
  option4: "All of these",
  ans: 2
},
{
  question: "What is the purpose of the <Badge> component in Reactstrap?",
  option1: "Represents a flexible and extensible container for content.",
  option2: "Used for creating visually appealing badges.",
  option3: "Defines a column within a Reactstrap layout.",
  option4: "None of these",
  ans: 2
},
{
  question: "How can you implement a collapsible accordion in Reactstrap?",
  option1: "Use the <Collapse> component to create a collapsible accordion.",
  option2: "Integrate the <Accordion> component.",
  option3: "Use the collapsible prop in the desired component.",
  option4: "Both (A) and (B)",
  ans: 1
},
{
  question: "What is the purpose of the <Nav> component in Reactstrap?",
  option1: "Represents a flexible and extensible container for content.",
  option2: "Used for creating navigation bars.",
  option3: "Both (A) and (B)",
  option4: "Defines a row within a Reactstrap layout.",
  ans: 2
},
{
  question: "How do you make a Reactstrap component hidden or visible based on a condition?",
  option1: "Use the hidden prop and set it to true or false.",
  option2: "Apply custom styles with the display property.",
  option3: "Both (A) and (B)",
  option4: " Reactstrap components cannot be conditionally hidden or visible.",
  ans: 2
},
{
  question: "How can you add tooltips to Reactstrap components?",
  option1: "Use the <Tooltip> component and set the tooltip prop to true.",
  option2: "Integrate the react-tooltip library.",
  option3: "Reactstrap components do not support tooltips.",
  option4: "All of these",
  ans: 2
},
{
  question: "What is the purpose of the <Spinner> component in Reactstrap?",
  option1: "Used for creating visually appealing badges.",
  option2: "Represents a flexible and extensible container for content.",
  option3: "Displays a loading spinner.",
  option4: "All of these",
  ans: 3
},
{
  question: "How do you add borders to a Reactstrap component?",
  option1: "Use the border prop and set it to true.",
  option2: "Apply custom styles with the border property.",
  option3: "Reactstrap components do not support borders.",
  option4: "None of these",
  ans: 2
},
{
  question: "What is the purpose of the <ListGroup> component in Reactstrap?",
  option1: "Represents a flexible and extensible container for content.",
  option2: "Used for creating navigation bars.",
  option3: "Organizes items into a list group.",
  option4: "None of these",
  ans: 3
},
{
  question: "How can you create a collapsible navbar in Reactstrap?",
  option1: "Integrate the <Accordion> component.",
  option2: " Reactstrap does not support collapsible navbars.",
  option3: "None of these",
  option4: "Use the <Collapse> component and toggle it with a state variable.",
  ans: 4
},
{
  question: "What is the purpose of the <Dropdown> component in Reactstrap?",
  option1: "Represents a flexible and extensible container for content.",
  option2: "Used for creating navigation bars.",
  option3: "Provides a dropdown menu.",
  option4: "None of these",
  ans: 3
},
{
  question: "How do you disable a specific item in a Reactstrap <Dropdown> component?",
  option1: "Set the disabled prop to true for the entire dropdown.",
  option2: " Use the disabled prop on the individual <DropdownItem> component.",
  option3: "Apply the disabled CSS class.",
  option4: "None of these",
  ans: 2
},
{
  question: "What is the purpose of the <Pagination> component in Reactstrap?",
  option1: "Used for creating navigation bars.",
  option2: "Represents a flexible and extensible container for content.",
  option3: "Organizes items into a pagination control.",
  option4: "All of these",
  ans: 3
},
{
  question: "How can you customize the style of a Reactstrap tooltip?",
  option1: "Use the customStyle prop on the <Tooltip> component.",
  option2: "Apply custom styles with the tooltipStyle property.",
  option3: "Reactstrap tooltips do not support custom styling.",
  option4: "Use the style prop on the <Tooltip> component.",
  ans: 4
}]