export const data = [
    {
        question : "Which type of JavaScript language is ?",
        option1 : "Object-Oriented",
        option2 : "Object-Based",
        option3 : "Assembly-language",
        option4 : "High-level",
        ans : 2
    },
    {
        question : "What is the purpose of the document.getElementById method in JavaScript?",
        option1 : "To get the value of an input field",
        option2 : "To retrieve an HTML element by its id",
        option3 : "To create a new HTML element",
        option4 : "To set the style of an element",
        ans : 2
    },
    {
        question : "Which one of the following also known as Conditional Expression?",
        option1 : "Alternative to if-else",
        option2 : "Switch statement",
        option3 : "If-then-else statement",
        option4 : "immediate if",
        ans : 4
    },
    {
        question : "Which keyword is used to declare a constant variable in JavaScript?",
        option1 : "var",
        option2 : "let",
        option3 : "const",
        option4 : "constant",
        ans : 3
    },
    {
        question : "What does the term 'SCOPE' refer to in JavaScript?",
        option1 : "The lifespan of a variable",
        option2 : "The visibility of a variable",
        option3 : "The type of a variable",
        option4 : "The size of a variable",
        ans : 2
    },
    {
        question : "What does the typeof operator return for an array in JavaScript?",
        option1 : "array",
        option2 : "object",
        option3 : "list",
        option4 : "type",
        ans : 2
    },
    {
        question : "What is the purpose of the setTimeout function in JavaScript?",
        option1 : "To define a timeout for asynchronous code",
        option2 : "To set the interval between function calls",
        option3 : "To delay the execution of a function",
        option4 : "To terminate a script",
        ans : 3
    },
    {
        question : "Which event is triggered when a user clicks on an HTML element?",
        option1 : "onhover",
        option2 : "onchange",
        option3 : "onclick",
        option4 : "onselect",
        ans : 3
    },
    {
        question : "What is the purpose of the Array.map method in JavaScript?",
        option1 : "To create a new array with the results of a function on each element",
        option2 : "To filter elements from an array",
        option3 : "To sort elements in an array",
        option4 : "To concatenate two arrays",
        ans : 1
    },
    {
        question : "How do you declare a function expression in JavaScript?",
        option1 : "function myFunction() {}",
        option2 : "var myFunction = function() {}",
        option3 : "const myFunction = () => {}",
        option4 : "Both b and c",
        ans : 4
    },
    {
        question : "Which keyword is used to prevent a variable from being modified in JavaScript?",
        option1 : "readonly",
        option2 : "const",
        option3 : "final",
        option4 : "immutable",
        ans : 2
    },
    {
        question : "What is the purpose of the JSON.stringify method in JavaScript?",
        option1 : "To parse JSON data",
        option2 : "To convert a JavaScript object to a JSON string",
        option3 : "To convert a JSON string to a JavaScript object",
        option4 : "To validate JSON syntax",
        ans : 2
    },
    {
        question : "Which operator is used for strict equality in JavaScript?",
        option1 : "==",
        option2 : "===",
        option3 : "=",
        option4 : "!=",
        ans : 2
    },
    {
        question : "What is the purpose of the Promise object in JavaScript?",
        option1 : "To represent a value that may be available now, in the future, or never",
        option2 : "To handle errors in asynchronous code",
        option3 : "To define a constant value",
        option4 : "To create a synchronous function",
        ans : 1
    },
    {
        question : "Which built-in function is used to sort elements in an array in JavaScript?",
        option1 : "arr.sort()",
        option2 : "arr.order()",
        option3 : "arr.sortElements()",
        option4 : "arr.orderBy()",
        ans : 1
    },
    {
        question : "What is the purpose of the localStorage object in JavaScript?",
        option1 : " To store data locally on the client's machine",
        option2 : "To send data to the server",
        option3 : "To store session data on the server",
        option4 : "To store data in cookies",
        ans : 1
    },
    {
        question : "Which method is used to remove the last element from an array in JavaScript?",
        option1 : "arr.removeLast()",
        option2 : "arr.pop()",
        option3 : "arr.deleteLast()",
        option4 : "arr.splice()",
        ans : 2
    },
    {
        question : "In JavaScript, what is a block of statement?",
        option1 : "Conditional block",
        option2 : "block that combines a number of statements into a single compound statement",
        option3 : "both conditional block and a single statement",
        option4 : "block that contains a single statement",
        ans : 2
    },
    {
        question : "The 'function' and 'var' are known as:",
        option1 : "Keywords",
        option2 : "Data types",
        option3 : "Declaration statements",
        option4 : "Prototypes",
        ans : 3
    },
    {
        question : "In the JavaScript, which one of the following is not considered as an error:",
        option1 : "Syntax error",
        option2 : "Missing of semicolons",
        option3 : "Division by zeros",
        option4 : "Missing of Bracket",
        ans : 3
    },
    {
        question : "WWhat is the result of 5 + '5' in JavaScript?",
        option1 : 55,
        option2 : 10,
        option3 : "Error",
        option4 : 505,
        ans : 1
    },
    {
        question : "What is the purpose of the typeof operator in JavaScript?",
        option1 : "Check if a variable is defined",
        option2 : "Determine the type of a variable",
        option3 : "Compare two variable",
        option4 : "Concaenate strings",
        ans : 2
    },
    {
        question : "How do you comment a single line in JavaScript?",
        option1 : "// Comment",
        option2 : "/* Comment */",
        option3 : "<!-- Comment -->",
        option4 : "-- Comment --",
        ans : 4
    },
    {
        question : "What does the NaN value represent in JavaScript?",
        option1 : "Not a Number",
        option2 : "Negative and Null",
        option3 : "No Assignment",
        option4 : "Nullify and Negate",
        ans : 1
    },
    {
        question : "What is the purpose of the Array.push() method in JavaScript?",
        option1 : "Removes the last element from an array",
        option2 : "Adds one or more elements to the end of an array",
        option3 : "Reverses the order of elements in an array",
        option4 : "Checks if an array is empty",
        ans : 2
    },
    {
        question : "What is the purpose of the querySelector method in JavaScript?",
        option1 : "// Comment",
        option2 : "/* Comment */",
        option3 : "<!-- Comment -->",
        option4 : "-- Comment --",
        ans : 4
    },
    {
        question : "What does the this keyword refer to in the global scope?",
        option1 : "Selects the first element with a specified class",
        option2 : "Adds a new element to the DOM",
        option3 : "Appends text to an existing element",
        option4 : "Creates a new array from an existing array",
        ans : 1
    },
    {
        question : "What does the innerHTML property do in JavaScript?",
        option1 : "Modifies the text content of an HTML element",
        option2 : " Retrieves the HTML content of an element",
        option3 : "Changes the style of an element",
        option4 : "Adds a new HTML element to the document",
        ans : 2
    },
    {
        question : "Which JavaScript feature is used to hold and manipulate text?",
        option1 : "Arrays",
        option2 : "Strings",
        option3 : " Objects",
        option4 : "Numbers",
        ans : 2
    },
    {
        question : "If a function which does not return a value is known as _____",
        option1 : "Static function",
        option2 : "Procedures",
        option3 : "Method",
        option4 : "Dynamic function",
        ans : 2
    },
    

];