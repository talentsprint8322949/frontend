export const  data =[{
  
    question: "What is Bootstrap?",
    option1: "A JavaScript library",
    option2: "A front-end framework",
    option3: "A server-side language",
    option4: "A database management system",
    ans: 2
  },
  {
    question: "In Bootstrap, what class is used to create a button?",
    option1: ".btn",
    option2: ".button",
    option3: ".btn-create",
    option4: ".button-style",
    ans: 1
  },
  {
    question: "Which Bootstrap class is used to create a responsive, fixed-width container?",
    option1: ".container-fluid",
    option2: ".container-fixed",
    option3: ".container",
    option4: ".fixed-container",
    ans: 3
  },
  {
    question: "How can you make a navigation bar responsive in Bootstrap?",
    option1: "Use the .nav-collapse class",
    option2: "Apply the .responsive-nav class",
    option3: "Wrap it with a <nav> element",
    option4: "Use the .navbar class",
    ans: 4
  },
  {
    question: "Which class is used to create a horizontal form in Bootstrap?",
    option1: ".form-horizontal",
    option2: ".horizontal-form",
    option3: ".form-inline",
    option4: ".inline-form",
    ans: 1
  },
  {
    question: "What is the purpose of the Bootstrap grid system?",
    option1: "Styling text elements",
    option2: "Creating responsive layouts",
    option3: "Adding images to the page",
    option4: "Applying colors to the background",
    ans: 2
  },
  {
    question: "Which class is used to add a background color to a Bootstrap alert?",
    option1: ".alert-color",
    option2: ".alert-background",
    option3: ".bg-alert",
    option4: ".alert",
    ans: 4
  },
  {
    question: "What is the purpose of the .img-fluid class in Bootstrap?",
    option1: "To make text fluid",
    option2: "To make images responsive",
    option3: "To create fluid containers",
    option4: "To add fluid animations",
    ans: 2
  },
  {
    question: "Which utility class in Bootstrap is used to hide an element visually but keep it available for screen readers?",
    option1: ".hidden",
    option2: ".invisible",
    option3: ".sr-only",
    option4: ".screen-reader",
    ans: 3
  },
  {
    question: "How can you make an image rounded in Bootstrap?",
    option1: ".rounded-img",
    option2: ".img-rounded",
    option3: ".round-img",
    option4: ".img-round",
    ans: 2
  },
  {
    question: "Which class is used to create a responsive table in Bootstrap?",
    option1: ".responsive-table",
    option2: ".table-responsive",
    option3: ".table-fluid",
    option4: ".responsive",
    ans: 2
  },
  {
    question: "In Bootstrap, what class is used to create a jumbotron?",
    option1: ".jumbotron",
    option2: ".big-box",
    option3: ".hero-unit",
    option4: ".jumbotron-box",
    ans: 1
  },
  {
    question: "Which class is used to create a badge in Bootstrap?",
    option1: ".badge",
    option2: ".label",
    option3: ".tag",
    option4: ".badge-label",
    ans: 1
  },
  {
    question: "What is the purpose of the .navbar-toggler class in Bootstrap?",
    option1: "To toggle the visibility of the navbar",
    option2: "To toggle the navbar's background color",
    option3: "To toggle the navbar's position",
    option4: "To toggle the navbar's font size",
    ans: 1
  },
  {
    question: "Which class is used to create a progress bar in Bootstrap?",
    option1: ".progressbar",
    option2: ".progress",
    option3: ".progress-bar",
    option4: ".bar",
    ans: 3
  },
  {
    question: " What does the .container-fluid class in Bootstrap do?",
    option1: "Creates a fixed-width container",
    option2: "Creates a full-width container",
    option3: "Creates a container with fluid text",
    option4: "Creates a container with rounded corners",
    ans: 2
  },
  {
    question: "Which class is used to create a card in Bootstrap?",
    option1: ".box",
    option2: ".card",
    option3: ".panel",
    option4: ".container-card",
    ans: 2
  },
  {
    question: "In Bootstrap, how can you make an element invisible without affecting its layout?",
    option1: ".invisible",
    option2: ".hidden",
    option3: ".opacity-0",
    option4: ".display-none",
    ans: 1
  },
  {
    question: "Which class is used to create a justified navigation menu in Bootstrap?",
    option1: ".nav-justified",
    option2: ".nav-right",
    option3: ".nav-align",
    option4: ".nav-fill",
    ans: 1
  },
  {
    question: "How can you add spacing between columns in the Bootstrap grid system?",
    option1: ".column-spacing",
    option2: ".column-margin",
    option3: ".column-padding",
    option4: ".gutter",
    ans: 4
  },
  {
    question: "What is the purpose of the .breadcrumb class in Bootstrap?",
    option1: "To create a navigation bar",
    option2: "To display hierarchical navigation",
    option3: "To style buttons",
    option4: "To add breadcrumbs to the footer",
    ans: 2
  },
  {
    question: "Which class is used to create a thumbnail in Bootstrap?",
    option1: ".box",
    option2: ".thumbnail",
    option3: ".image-box",
    option4: ".img-thumbnail",
    ans: 4
  },
  {
    question: "How can you vertically align content within a Bootstrap grid column?",
    option1: ".vertical-align",
    option2: ".align-middle",
    option3: ".center-content",
    option4: ".v-align",
    ans: 2
  },
  {
    question: "What is the purpose of the .text-muted class in Bootstrap?",
    option1: "To create bold text",
    option2: "To add a muted background color",
    option3: "To style text with a muted color",
    option4: "To hide text content",
    ans: 3
  },
  {
    question: "Which class is used to create a modal in Bootstrap?",
    option1: ".modal-box",
    option2: ".popup",
    option3: ".modal",
    option4: ".popup-box",
    ans: 3
  },
  {
    question: "What is the purpose of the .list-group class in Bootstrap?",
    option1: "To create a navigation menu",
    option2: "To style lists",
    option3: "To add borders to elements",
    option4: "To create a group of list items",
    ans: 4
  },
  {
    question: "How can you make an image rounded in Bootstrap?",
    option1: ".rounded-img",
    option2: ".img-rounded",
    option3: ".round-img",
    option4: ".img-round",
    ans: 2
  },
  {
    question: " Which class is used to create a responsive, full-width image in Bootstrap?",
    option1: ".full-width-img",
    option2: ".img-responsive",
    option3: ".wide-img",
    option4: ".responsive-img",
    ans: 2
  },
  {
    question: "In Bootstrap, what class is used to create a badge?",
    option1: ".badge",
    option2: ".tag",
    option3: ".label",
    option4: ".badge-label",
    ans: 1
  },
  {
    question: "Which class is used to create a collapsible accordion in Bootstrap?",
    option1: ".accordion",
    option2: ".collapse",
    option3: ".collapse-accordion",
    option4: ".accordion-collapse",
    ans: 2
  }]