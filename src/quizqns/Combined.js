// QuizContainer.js
import React, { useState } from 'react';
import Test from '../quizdata/Test';
import Quiz_css from './Quiz_css';

const Combined = () => {
  const [testActive, setTestActive] = useState(true);

  const handleTimerEnd = () => {
    setTestActive(false);
    alert('Test Ended!');
  };

  return (
    <div>
      {testActive ? (
        <div>
          <Test />
          <Quiz_css onTimerEnd={handleTimerEnd} />
        </div>
      ) : (
        <div>
          {/* Display any result or summary when the test is completed */}
        </div>
      )}
    </div>
  );
};

export default Combined;
