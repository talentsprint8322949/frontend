export const  data =
[{
  question: "JDK stands for ____.",
  option1: "Java development kit",
  option2: "Java deployment kit",
  option3: "JavaScript deployment kit",
  option4: "None of these",
  ans: 1
},
{
 
  question: "JRE stands for ___.",
  option1: "Java run ecosystem",
  option2: "JDK runtime Environment",
  option3: "Java Runtime Environment",
  option4: "None of these",
  ans: 3
},
{

  question: "What makes the Java platform independent?",
  option1: "Advanced programming language",
  option2: "It uses bytecode for execution",
  option3: "Class compilation",
  option4: "All of these",
  ans: 2
},
{
 
  question: "What are the types of memory allocated in memory in java?",
  option1: "Heap memory",
  option2: "Stack memory",
  option3: "Both A and B",
  option4: "None of these",
  ans: 3
},
{
  
  question: "Multiline comment is created using ___.",
  option1: "//",
  option2: "/* */",
  option3: "Both A and B",
  option4: "All of these",
  ans: 2
},
{
 
  question: " What is the entry point of a program in Java?",
  option1: "main() method",
  option2: "The first line of code",
  option3: "Last line of code",
  option4: "main class",
  ans: 1
},
{
 
  question: " Which keyword in java is used for exception handling?",
  option1: "exep",
  option2: "excepHand",
  option3: "throw",
  option4: "All of these",
  ans: 3
},
{
 
  question: "Which class in Java is used to take input from the user?",
  option1: "Scanner",
  option2: "Input",
  option3: "Applier",
  option4: "None of these",
  ans: 1
},
{

  question: "Method used to take a string as input in Java?",
  option1: "next()",
  option2: "nextLine()",
  option3: "Both A. and B.",
  option4: "None of these",
  ans: 3
},
{
 
  question: "Which of the following is the correct syntax to create a variable in Java?",
  option1: "var name;",
  option2: "int name;",
  option3: "var name int;",
  option4: "All of these",
  ans: 2
},
{
 
  question: "Which of these is a type of variable in Java?",
  option1: "Instance Variable",
  option2: "Local Variable",
  option3: "Static Variable",
  option4: "All of these",
  ans: 4
},
{
 
  question: "What is type casting in Java?",
  option1: "It is converting type of a variable from one type to another",
  option2: "Casting variable to the class",
  option3: "Creating a new variable",
  option4: "All of these",
  ans: 1
},
{
  
  question: "Which type of casting is lossy in Java?",
  option1: "Widening typecasting",
  option2: "Narrowing typecasting",
  option3: "Manual typecasting",
  option4: "All of these",
  ans: 2
},
{
  
  question: "Which of the following can be declared as final in java?",
  option1: "Class",
  option2: "Method",
  option3: "Variable",
  option4: "All of these",
  ans: 4
},
{
 
  question: "Finally block is attached to?",
  option1: "Try-catch block",
  option2: "Class block",
  option3: "Method block",
  option4: "All of these",
  ans: 1
},
{

  question: "The break statement in Java is used to ___.",
  option1: "Terminates from the loop immediately",
  option2: "Terminates from the program immediately",
  option3: "Skips the current iteration",
  option4: "All of these",
  ans: 1
},
{
 
  question: "Can the Java program accept input from the command line?",
  option1: "Yes, using command-line arguments",
  option2: "Yes, by access command prompt",
  option3: "No",
  option4: "None of these",
  ans: 1
},
{
 
  question: "Array in java is ___.",
  option1: "Collection of similar elements",
  option2: "Collection of elements of different types",
  option3: "The data type of consisting of characters",
  option4: "None of these",
  ans: 1
},
{
 
  question: "Which of these is the correct method to create an array in java?",
  option1: "int[] arr = {1, 3, 5}; ",
  option2: "arr = new int[] {3, 1, 8};",
  option3: "int arr[] = {1, 4, 6};",
  option4: "All of these",
  ans: 4
},
{
 
  question: "Object in java are ___.",
  option1: "Classes ",
  option2: "References",
  option3: "Iterators",
  option4: "None of these",
  ans: 2
},
{
 
  question: "What is garbage collection in java?",
  option1: "Method to manage memory in java ",
  option2: "Create new garbage values",
  option3: "Delete all values",
  option4: "All of these",
  ans: 1
},
{
 
  question: "Static variables in java are declared as ___.",
  option1: "final variables",
  option2: "new variables",
  option3: "Constants",
  option4: "All of these",
  ans: 3
},
{
 
  question: "BigInteger Class is used to ___.",
  option1: "Store very long range of number",
  option2: "Store integer values",
  option3: "A class that stores large range of integer",
  option4: "All of these",
  ans: 4
},
{
 
  question: "'this' keyword in java is ___.",
  option1: "Used to hold the reference of the current object",
  option2: "Holds object value",
  option3: "Used to create a new instance",
  option4: "All of these",
  ans: 1
},
{
 
  question: "The 'super' keyword is used to ___.",
  option1: "Access instance of the parent class",
  option2: "Access instance of the same class",
  option3: "Access instance of child class",
  option4: "Access instance of friend class",
  ans: 1
},
{
  
  question: "The super() method is used to ___.",
  option1: "Call constructor of friend class",
  option2: "Is a declared method",
  option3: "Call constructor of the parent class",
  option4: "Call constructor",
  ans: 3
},
{
 
  question: "Wrapper class in java is ___.",
  option1: "Used to encapsulate primitive data types",
  option2: "Declare new classes called wrapper",
  option3: "Create a new instance of the class",
  option4: "None of these",
  ans: 1
},
{
 
  question: "Boxing is ___.",
  option1: "Creating new box",
  option2: "Creating object",
  option3: "Converting primitive type of object instance",
  option4: "None of these",
  ans: 3
},
{
  
  question: "Abstract class is ___.",
  option1: "Created using abstract keyword",
  option2: "Contains only abstract method",
  option3: "Needs to be inherited to be used",
  option4: "All of these",
  ans: 4
},
{

  question: "What is file handling in java?",
  option1: "It is creating, deleting, and modifying files using a java program.",
  option2: "Creating new method",
  option3: "Filing method to different file to extract them better",
  option4: "All of these",
  ans: 1
}]