export const data = [{
  
    question: "Node.js is written in",
    option1: "JavaScript",
    option2: "C",
    option3: "C++",
    option4: "All of the above",
    ans: 4
  },
  {
    
    question: "How modules in Node.js can be connected from one component to another ?",
    option1: "Expose",
    option2: "Module",
    option3: "Exports",
    option4: "All of the above",
    ans: 3
  },
  {
   
    question: "What is Node.js?",
    option1: "A database management system",
    option2: "A JavaScript runtime built on Chrome's V8 JavaScript engine",
    option3: "An operating system",
    option4: "none of these",
    ans: 2
  },
  {
    
    question: "Which module is used for handling file operations in Node.js",
    option1: "fs",
    option2: "http",
    option3: "net",
    option4: "all of these",
    ans: 1
  },
  {
    
    question: "What is the purpose of the npm package manager in Node.js?",
    option1: "Node.js Plugin Manager",
    option2: "Node Project Manager",
    option3: "Node Package Manager",
    option4: "all of these",
    ans: 3
  },
  {
    
    question: "Which HTTP method is used to request data from a server in Node.js?",
    option1: "POST",
    option2: "GET",
    option3: "DELETE",
    option4: "none of these",
    ans: 2
  },
  {
    
    question: "What does the term Event-driven mean in the context of Node.js?",
    option1: "Code execution based on events",
    option2: "Synchronous execution only",
    option3: " No support for events",
    option4: "all of these",
    ans: 1
  },
  {
    
    question: "Which module is used for building web servers in Node.js?",
    option1: "fs",
    option2: "http",
    option3: "url",
    option4: "all of these",
    ans: 2
  },
  {
    
    question: "What is the purpose of the express framework in Node.js?",
    option1: "Database management",
    option2: "Web application framework",
    option3: "File handling",
    option4: "none of these",
    ans: 2
  },
  {
   
    question: "Which command is used to install dependencies from the package.json file in Node.js?",
    option1: "npm start",
    option2: "npm install",
    option3: "npm run",
    option4: "all of these",
    ans: 2
  },
  {
    
    question: "How can you handle asynchronous operations in Node.js?",
    option1: "Using callbacks",
    option2: "Using only synchronous functions",
    option3: "Using promises",
    option4: "all of these",
    ans: 1
  },
  {
    question: "What is the purpose of the global object in Node.js?",
    option1: "Access to global variables",
    option2: "Access to local variables",
    option3: "File system operations",
    option4: "none of these",
    ans: 1
  },
  {
    
    question: "Which tool is used for managing Node.js versions?",
    option1: "Node Manager (NM)",
    option2: "Node Version Manager (NVM)",
    option3: "Node Version Control (NVC)",
    option4: "all of these",
    ans: 2
  },
  {
    
    question: "What is the purpose of the Buffer class in Node.js?",
    option1: "File compression",
    option2: "Handling binary data",
    option3: "DOM manipulation",
    option4: "all of these",
    ans: 2
  },
  {
    
    question: "How can you pass data between middleware functions in Express.js?",
    option1: "Using global variables",
    option2: "Using request parameters",
    option3: "Using req and res objects",
    option4: "none of these",
    ans: 3
  },
  {
    
    question: "Which npm command is used to uninstall a package in Node.js?",
    option1: "npm remove",
    option2: "npm delete",
    option3: "npm uninstall",
    option4: "all of these",
    ans: 3
  },
  {
    
    question: "What is the purpose of the process object in Node.js?",
    option1: "Managing file processes",
    option2: "Interacting with the operating system",
    option3: "DOM manipulation",
    option4: "none of these",
    ans: 2
  },
  {
    
    question: "Which module is used for handling query strings in Node.js?",
    option1: "query",
    option2: "url",
    option3: "qs",
    option4: "all of these",
    ans: 2
  },
  {
    
    question: "What is the purpose of the cluster module in Node.js?",
    option1: "Managing multiple processes",
    option2: "Database clustering",
    option3: "File compression",
    option4: "all of these",
    ans: 1
  },
  {
   
    question: "Which npm command is used to view installed packages and their versions in Node.js?",
    option1: "npm list",
    option2: "npm show",
    option3: "npm info",
    option4: "all of these",
    ans: 1
  },
  {
    
    question: "How can you make a Node.js script run automatically whenever a file changes?",
    option1: "Using node-watch package",
    option2: "Using nodemon package",
    option3: "Using node-auto-reload",
    option4: "all of these",
    ans: 2
  },
  {
    
    question: "Which method is used to perform a non-blocking sleep in Node.js?",
    option1: "setTimeout",
    option2: "sleep",
    option3: "wait",
    option4: "none of these",
    ans: 1
  },
  {
    question: "What is the purpose of the module.exports object in Node.js?",
    option1: "Importing modules",
    option2: "Exporting functions or objects from a module",
    option3: "File compression",
    option4: "none of these",
    ans: 2
  },
  {
   
    question: "Which HTTP status code represents a successful request in Node.js?",
    option1: "200 OK",
    option2: "404 Not Found",
    option3: "500 Internal Server Error",
    option4: "all of these",
    ans: 1
  },
  {
   
    question: "What is the purpose of the middleware in Express.js?",
    option1: "Managing file processes",
    option2: "Interacting with the operating system",
    option3: "Executing functions during the request-response cycle",
    option4: "none of these",
    ans: 3
  },
  {
   
    question: "How can you handle errors in asynchronous code in Node.js?",
    option1: "Using try-catch blocks",
    option2: "Using only callbacks",
    option3: "Errors cannot be handled in asynchronous code",
    option4: "all of these",
    ans: 1
  },
  {
   
    question: "What is the purpose of the cookie-parser middleware in Express.js?",
    option1: "Parsing JSON data",
    option2: "Parsing URL-encoded data",
    option3: "Parsing cookies from incoming requests",
    option4: "none of these",
    ans: 3
  },
  {
   
    question: "Which npm command is used to install a package globally?",
    option1: "npm add",
    option2: "npm global install",
    option3: "npm install -g",
    option4: "all of these",
    ans: 3
  },
  {
    
    question: "What is the purpose of the body-parser middleware in Express.js?",
    option1: "Parsing JSON data",
    option2: "Parsing URL-encoded data",
    option3: "Parsing cookies from incoming requests",
    option4: "all of these",
    ans: 2
  },
  {
    
    question: "How can you secure Express.js applications against common web vulnerabilities?",
    option1: "Using HTTPS only",
    option2: "Using helmet middleware",
    option3: "Disabling all middleware",
    option4: "none of these",
    ans: 2
  },
  {
   
    question: "Which method is used to handle HTTP DELETE requests in Express.js?",
    option1: "app.post()",
    option2: "app.delete()",
    option3: "app.get()",
    option4: "all of these",
    ans: 2
  },
  {
    question: "What is the purpose of the dotenv package in Node.js?",
    option1: "Database connection",
    option2: "Loading environment variables from a .env file",
    option3: "DOM manipulation",
    option4: "none of these",
    ans: 2
  },
  {
    
    question: "Which npm command is used to update all packages to their latest versions in Node.js?",
    option1: "npm upgrade",
    option2: "npm update",
    option3: "npm install --latest",
    option4: "none of these",
    ans: 2
  },
  {
   
    question: "How can you create a simple HTTP server using the http module in Node.js?",
    option1: "Using http.createServer()",
    option2: "Using express.createServer()",
    option3: "Using server.create()",
    option4: "all of these",
    ans: 1
  },
  {
    
    question: "What is the purpose of the mongoose library in Node.js?",
    option1: "File manipulation",
    option2: "Database connection and modeling for MongoDB",
    option3: "HTTP request handling",
    option4: "none of these",
    ans: 2
  }
  ]