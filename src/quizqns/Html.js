export const data =
[{
    question: "What is HTML?",
    option1: "HTML describes the structure of a webpage",
    option2: "HTML is the standard markup language mainly used to create web pages",
    option3: " HTML consists of a set of elements that helps the browser how to view the content",
    option4: "All of the mentioned",
    ans: 4
  },
  {
    question: "Who is the father of HTML?",
    option1: "Rasmus Lerdorf",
    option2: "Tim Berners-Lee",
    option3: " Brendan Eich",
    option4: "Sergey Brin",
    ans: 2
  },
  {
    question: "HTML stands for __________",
    option1: "HyperText Markup Language",
    option2: "HyperText Machine Language",
    option3: " HyperText Marking Language",
    option4: "HighText Marking Language",
    ans: 1
  },
  {
    question: "What is the correct syntax of doctype in HTML5?",
    option1: "</doctype html>",
    option2: "<doctype html>",
    option3: "<doctype html!>",
    option4: "<!doctype html>",
    ans: 4
  },
  {
    question: "Which of the following is used to read an HTML page and render it?",
    option1: "Web server",
    option2: "Web network",
    option3: "Web browser",
    option4: "Web matrix",
    ans: 3
  },
  {
    question: "Which of the following tag is used for inserting the largest heading in HTML?",
    option1: "head",
    option2: "<h1>",
    option3: "<h6>r",
    option4: "heading",
    ans: 2
  },
  {
    question: "What is DOM in HTML?",
    option1: "Language dependent application programming",
    option2: "Application programming interface",
    option3: "Convention for representing and interacting with objects in html documentsr",
    option4: "Hierarchy of objects in ASP.NET",
    ans: 3
  },
  {
    question: "In which part of the HTML metadata is contained?",
    option1: "head tag",
    option2: "title tag",
    option3: "html tag",
    option4: "body tag",
    ans: 1
  },
  {
    question: "Which element is used to get highlighted text in HTML5?",
    option1: "<u>",
    option2: "<mark>",
    option3: "<highlight>",
    option4: "<b>",
    ans: 2
  },
  {
    question: "Which of the following is not a HTML5 tag?",
    option1: "<track>",
    option2: "<video>",
    option3: "<slider>",
    option4: "<source>",
    ans: 3
  },
  {
    question: "How do we write comments in HTML?",
    option1: "</…….>",
    option2: "<!……>",
    option3: "</……/>",
    option4: "<…….!>",
    ans: 2
  },
  {
    question: "Which of the following elements in HTML5 defines video or movie content?",
    option1: "<video>",
    option2: "<movie>",
    option3: "<audio>",
    option4: "<media>",
    ans: 1
  },
  {
    question: "Which of the following is not the element associated with the HTML table layout?",
    option1: "alignment",
    option2: "color",
    option3: "size",
    option4: "spanning",
    ans: 2
  },
  {
    question: "Which element is used for or styling HTML5 layout?",
    option1: "CSS",
    option2: "jQuery",
    option3: "JavaScript",
    option4: "PHP",
    ans: 1
  },
  {
    question: "Which HTML tag is used for making character appearance bold?",
    option1: "<u>content</u>",
    option2: "<b>content</b>",
    option3: "<br>content</br>",
    option4: "<i>content</i>",
    ans: 2
  },
  {
    question: "Which HTML tag is used to insert an image?",
    option1: "<img url=”htmllogo.jpg” />",
    option2: "<img alt=”htmllogo.jpg” />",
    option3: "<img src=”htmllogo.jpg” />",
    option4: "<img link=”htmllogo.jpg” />",
    ans: 3
  },
  {
    question: "HTML is a subset of ___________",
    option1: "SGMT",
    option2: "SGML",
    option3: "SGME",
    option4: "XHTML",
    ans: 2
  },
  {
    question: "Among the following, which is the HTML paragraph tag?",
    option1: "<p>",
    option2: "<pre>",
    option3: "<hr>",
    option4: "<a>",
    ans: 1
  },
  {
    question: "In HTML, which attribute is used to create a link that opens in a new window tab?",
    option1: "src=”_blank”",
    option2: "alt=”_blank”",
    option3: "target=”_self”",
    option4: "target=”_blank”",
    ans: 4
  },
  {
    question: "Which HTML element is used for short quote?",
    option1: "<em>",
    option2: "<abbr>",
    option3: "<q>",
    option4: "<blockquote>",
    ans: 3
  },
  {
    question: "Which of the following HTML tag is used to create an unordered list?",
    option1: "<ol>",
    option2: "<ul>",
    option3: "<li>",
    option4: "<ll>",
    ans: 2
  },
  {
    question: " Which HTML element is used for abbreviation or acronym?",
    option1: "<abbr>",
    option2: "<blockquote>",
    option3: "<q>",
    option4: "<em>",
    ans: 1
  },
  {
    question: "Which of the following HTML tag is used to add a row in a table?",
    option1: "<th>",
    option2: "<td>",
    option3: "<tr>",
    option4: "<tt>",
    ans: 3
  },
  {
    question: "What is the work of <address> element in HTML5?",
    option1: "contains IP address",
    option2: "contains home address",
    option3: "contains url",
    option4: "contains contact details for author",
    ans: 4
  },
  {
    question: "Which of the following tag is used to create a text area in HTML Form?",
    option1: "<textarea> </textarea>",
    option2: "<text></text>",
    option3: "<input type=”text” />",
    option4: "<input type=”textarea” />",
    ans: 1
  },
  {
    question: "To show deleted text, which HTML element is used?",
    option1: "<del>",
    option2: "<em>",
    option3: "<strong>",
    option4: "<ins>",
    ans: 1
  },
  {
    question: "Which tag is used to create a dropdown in HTML Form?",
    option1: "<input>",
    option2: "<select>",
    option3: "<text>",
    option4: "<textarea>",
    ans: 2
  },
  {
    question: "Which tag is used to create a numbered list in HTML?",
    option1: "<ol>",
    option2: "<ul>",
    option3: "<li>",
    option4: "<ll>",
    ans: 1
  },
  {
    question: "How to create a checkbox in HTML Form?",
    option1: "<input type=”text”>",
    option2: "<input type=”textarea”>",
    option3: "<input type=”checkbox”>",
    option4: "<input type=”button”>",
    ans: 3
  },
  {
    question: "Which of the following extension is used to save an HTML file?",
    option1: ".hl",
    option2: ".h",
    option3: ".htl",
    option4: ".html",
    ans: 4
  },
  {
    question: "Which tag is used to create a blank line in HTML?",
    option1: "<b>",
    option2: "<br>",
    option3: "<em>",
    option4: "<a>",
    ans: 2
  },
  {
    question: "Which HTML tag is used to convert the plain text into italic format?",
    option1: "<b>",
    option2: "<p>",
    option3: "<i>",
    option4: "<a>",
    ans: 3
  },
  {
    question: "What is the use of <hr/> tag in HTML?",
    option1: "For making content appearance italics",
    option2: "To create vertical rule between sections",
    option3: "To create a line break",
    option4: "To create horizontal rule between sections",
    ans: 4
  },
  {
    question: "Which attribute is not essential under <iframe>?",
    option1: "frameborder",
    option2: "width",
    option3: "height",
    option4: "src",
    ans: 1
  },
  {
    question: "Which works similar to <b> element?",
    option1: "<blockquote>",
    option2: "<strong>",
    option3: "<em>",
    option4: "<i>",
    ans: 2
  }]