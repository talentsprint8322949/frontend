 export const data = [{
  
    question: "What is JSX in React?",
    option1: "JavaScript Syntax extension",
    option2: "Java XML",
    option3: "JavaScript XML",
    option4: "JSX is not used in React",
    ans: 3
  },
  {
    
    question: "What is the purpose of React's virtual DOM?",
    option1: "To improve rendering performance",
    option2: "To create virtual components",
    option3: "To replace the actual DOM",
    option4: "To enhance styling in React",
    ans: 1
  },
  {
    
    question: "What is the significance of state in React?",
    option1: "It is used for storing external data",
    option2: "It is used for mutable data that can change over time",
    option3: "It is used for defining constants",
    option4: "It is used for handling routing in React",
    ans: 2
  },
  {
    
    question: "What is the purpose of React Router?",
    option1: "To manage HTTP requests in React applications",
    option2: "To navigate between different components in a React application",
    option3: "To handle state in React components",
    option4: "To create animations in React",
    ans: 2
  },
  {
   
    question: "What are props in React?",
    option1: "Local variables in a component",
    option2: "Parameters passed to a component",
    option3: "Internal state of a component",
    option4: "A type of HTML element in React",
    ans: 2
  },
  {
    
    question: "What is the purpose of the useEffect hook in React?",
    option1: "To handle mouse events in React components",
    option2: "To manage asynchronous operations in React components",
    option3: "To create custom hooks",
    option4: "To manage side effects in functional components",
    ans: 4
  },
  {
   
    question: "What is the role of the setState method in React?",
    option1: "To define the initial state of a component",
    option2: "To update the state of a component",
    option3: "To reset the state of a component",
    option4: "To handle external API calls in React",
    ans: 2
  },
  {
    
    question: "What is the purpose of the React context API?",
    option1: "To manage global state in a React application",
    option2: "To create context menus in React components",
    option3: "To handle HTTP requests",
    option4: "To define styles for React components",
    ans: 1
  },
  {
    
    question: "What is the significance of the PureComponent in React?",
    option1: "It is used for rendering pure HTML",
    option2: "It prevents unnecessary re-renders in functional components",
    option3: "It ensures that components always re-render",
    option4: "It is used for handling routing in React",
    ans: 2
  },
  {
    
    question: "How does React handle forms?",
    option1: "React doesn't support forms",
    option2: "Forms in React are handled using Redux",
    option3: "React uses controlled components to manage form state",
    option4: "React uses a separate library for form handling",
    ans: 3
  },
  {
    
    question: "What is React?",
    option1: "A programming language",
    option2: "A JavaScript library for building user interfaces",
    option3: "An operating system",
    option4: "A database management system",
    ans: 2
  },
  {
    
    question: "Which of the following is the correct way to create a React component?",
    option1: "const myComponent = new React.Component();",
    option2: "function MyComponent() {}",
    option3: "class MyComponent extends Component {}",
    option4: "createComponent(MyComponent);",
    ans: 3
  },
  {
    
    question: "What does the useState hook do in React?",
    option1: "Renders the component",
    option2: "Handles component state",
    option3: "Creates a new component",
    option4: "Imports external libraries",
    ans: 2
  },
  {
    
    question: "How can you pass data from a parent component to a child component in React?",
    option1: "Using the setState method",
    option2: "By declaring variables in the child component",
    option3: "Through props",
    option4: "Using the this.data property",
    ans: 3
  },
  {
    
    question: "What is the purpose of the componentDidMount lifecycle method in React?",
    option1: "Invoked after a component is created",
    option2: "Invoked before a component is unmounted",
    option3: "Invoked before rendering a component",
    option4: "Invoked during component re-render",
    ans: 1
  },
  {
   
    question: "Which method is used to handle events in React?",
    option1: "eventHandler",
    option2: "handleEvent",
    option3: "onClick",
    option4: "eventListener",
    ans: 3
  },
  {
    
    question: "What is React Router used for?",
    option1: "Managing state in React components",
    option2: "Routing and navigation in React applications",
    option3: "Connecting React to a database",
    option4: "Styling React components",
    ans: 2
  },
  {
   
    question: "What is the purpose of the key prop in React lists?",
    option1: "It defines the color of the list items",
    option2: "It provides a unique identifier for list items",
    option3: "It sets the font size of the list items",
    option4: "It determines the width of the list items",
    ans: 2
  },
  {
    
    question: "Which function is used to update the state in a React component?",
    option1: "setState",
    option2: "updateState",
    option3: "changeState",
    option4: "modifyState",
    ans: 1
  },
  {
    
    question: "What is the purpose of the map function in React?",
    option1: "It creates a map of elements",
    option2: "It iterates over an array and returns a new array",
    option3: "It draws maps on a React component",
    option4: "It maps CSS styles to React components",
    ans: 2
  },
  {
    
    question: "In React, what is the significance of the shouldComponentUpdate method?",
    option1: "It is used to determine if a component should be updated or not",
    option2: "It updates the component immediately",
    option3: "It controls the rendering process of child components",
    option4: "It checks for syntax errors in the component",
    ans: 1
  },
  {
   
    question: "What is a controlled component in React?",
    option1: "A component with strict access control",
    option2: "A component that manages its own state",
    option3: "A component with limited functionality",
    option4: "A component without event handlers",
    ans: 2
  },
  {
    
    question: "Which of the following is the correct way to handle forms in React?",
    option1: "Using the formHandler method",
    option2: "Setting state directly from the form input",
    option3: "Using the onChange event and updating state",
    option4: "Ignoring form events in React",
    ans: 3
  },
  {
   
    question: "What is the purpose of the useEffect hook in React?",
    option1: "To handle side effects in functional components",
    option2: "To create effects like animations",
    option3: "To manage the component's state",
    option4: "To handle AJAX requests",
    ans: 1
  },
  {
   
    question: "Which of the following is the correct name of React.js?",
    option1: "React",
    option2: "React.js",
    option3: "ReactJS",
    option4: "All of the above",
    ans: 4
  },
  {
   
    question: "Which of the following is not a disadvantage of React.js?",
    option1: "React.js has only a view layer. We have put your code for Ajax requests, events and so on.",
    option2: "The library of React.js is pretty large.",
    option3: "The JSX in React.js makes code easy to read and write.",
    option4: "The learning curve can be steep in React.js.",
    ans: 3
  },
  {
    
    question: "A class is a type of function, but instead of using the keyword function to initiate it, which keyword do we use?",
    option1: "Constructor.",
    option2: "Class",
    option3: "Object",
    option4: "DataObject",
    ans: 2
  },
  {
    
    question: "How does React handle data binding?",
    option1: "Unidirectional data flow",
    option2: "Bidirectional data flow",
    option3: "No data binding in React",
    option4: "Automatic data synchronization",
    ans: 1
  },
  {
    
    question: "What is JSX and why is it used in React?",
    option1: "JavaScript Extension for XML, used for creating XML documents",
    option2: "JavaScript XML, a syntax extension for JavaScript that looks similar to XML/HTML",
    option3: "JavaScript Syntax for XML, used for declaring XML namespaces",
    option4: "JavaScript XML, a JavaScript library for XML manipulation",
    ans: 2
  },
  {
    
    question: "Which of the following acts as the input of a class-based component?",
    option1: "Class",
    option2: "Factory",
    option3: "Render",
    option4: "Props",
    ans: 4
  },
  {
    
    question: "How many numbers of elements a valid react component can return?",
    option1: "1",
    option2: "2",
    option3: "4",
    option4: "5",
    ans: 1
  },
  {
    
    question: "What are the two ways to handle data in React?",
    option1: "State & Props",
    option2: "Services & Components",
    option3: "State & Services",
    option4: "State & Component",
    ans: 1
  },
  {
    
    question: "Which of the following function is called to render HTML to the web page in React?",
    option1: "render()",
    option2: "render()",
    option3: "ReactDOM_render()",
    option4: "render()",
    ans: 1
  },
  {
    question: "What is the significance of the PureComponent in React?",
    option1: "It is used for rendering pure HTML",
    option2: "It prevents unnecessary re-renders in functional components",
    option3: "It ensures that components always re-render",
    option4: "It is used for handling routing in React",
    ans: 2
  },
  {
    
    question: "In React, what is the significance of the shouldComponentUpdate method?",
    option1: "It is used to determine if a component should be updated or not",
    option2: "It updates the component immediately",
    option3: "It controls the rendering process of child components",
    option4: "It checks for syntax errors in the component",
    ans: 1
  }]