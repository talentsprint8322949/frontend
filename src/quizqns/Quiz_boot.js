import React, { useRef, useState, useEffect } from 'react';

import { data } from './bootstrap.js';
import { Link } from 'react-router-dom';
import QuizTimer from '../Quiztimer.js';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Quiz_boot = () => {
    const [questions, setQuestions] = useState([]);
    const [index, setIndex] = useState(0);
    const [question, setQuestion] = useState(questions[0] || null); // Initialize with the first question or null
    const [lock, setLock] = useState(false);
    const [score, setScore] = useState(0);
    const [result, setResult] = useState(false);
    const [quizFinished, setQuizFinished] = useState(false);
    const [timeLeft, setLeft] = useState(true);
    let [testActive, setTestActive] = useState(true);

    const handleTimerEnd = () => {
        setTestActive(false);
        toast.success('Test Ended!', {
            position: toast.POSITION.BOTTOM_RIGHT,
            autoclose :100000,
            style: {
                width: '600px', 
              },
          });
        // alert('Test Ended!');
      };

    const Option1 = useRef(null);
    const Option2 = useRef(null);
    const Option3 = useRef(null);
    const Option4 = useRef(null);
    const optionArray = [Option1, Option2, Option3, Option4];

    // Shuffle function to randomize the questions
    const shuffleArray = (array) => {
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
        return array;
    };

    useEffect(() => {
        // Generate 10 random questions
        const shuffledQuestions = shuffleArray(data).slice(0, 10);
        setQuestions(shuffledQuestions);
        setQuestion(shuffledQuestions[0]); // Initialize with the first question
    }, []);

    const checkAns = (e, ans) => {
        if (lock === false && question && question.ans === ans) {
            e.target.classList.add('correct');
            setLock(true);
            setScore((prev) => prev + 1);
        } else if (lock === false && question) {
            e.target.classList.add('wrong');
            setLock(true);
            optionArray[question.ans - 1].current.classList.add('correct');
        }
    };

    const finishQuiz = () => {
        // Add any additional logic you need when finishing the quiz
        console.log('Quiz Finished');
    };

    const next = () => {
        if (lock === true) {
            if (index === questions.length - 1) {
                setResult(true);
                setQuizFinished(true);
                handleTimerEnd();
            }
            setIndex((prevIndex) => prevIndex + 1);
            setQuestion(questions[index + 1]);
            setLock(false);
            optionArray.forEach((option) => {
                option.current.classList.remove('wrong');
                option.current.classList.remove('correct');
            });
        }
    };

    return (
        <div className="container" style = {{backgroundColor : "skyblue"}}>
           <div style={{display:'flex',flexDirection:'row',justifyContent:'space-between'}}>
           <h1>Quiz APP</h1>
            {testActive && <QuizTimer onTimerEnd={handleTimerEnd} setLeft={timeLeft} />}
           </div>
            <hr />
            {result ? (
                <>
                    <h2>Thank you for attending the test</h2>
                    <h2>You scored {score} out of {questions.length} 🏅</h2>
                    <Link to="/Mulcards">
                        <button onClick={finishQuiz}>Finish</button>
                    </Link>
                </>
            ) : (
                <>
                    <h3>{index + 1}. {question && question.question}</h3>
                    <ul>
                    <li ref={Option1} onClick={testActive ? (e) =>{checkAns(e,1)} : ()=>{}}>{question &&question.option1}</li>
                        <li ref={Option2} onClick={testActive? (e) => checkAns(e, 2) : ()=>{}}>{question && question.option2}</li>
                        <li ref={Option3} onClick={testActive?(e) => checkAns(e, 3):()=>{}}>{question && question.option3}</li>
                        <li ref={Option4} onClick={testActive?(e) => checkAns(e, 4):()=>{}}>{question && question.option4}</li>
                    </ul>
                    <button onClick={next}>Next</button>
                    <div className="index">{index + 1} of {questions.length} questions</div>
                </>
            )}
            <ToastContainer />
        </div>
    );
};

export default Quiz_boot;
