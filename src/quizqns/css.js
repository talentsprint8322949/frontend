export const data = [

    {
  
    question: "What is CSS?",
    option1: "CSS is a style sheet language",
    option2: "CSS is designed to separate the presentation and content, including layout, colors, and fonts",
    option3: "CSS is the language used to style the HTML documents",
    option4: "All of the mentioned",
    ans: 4
  },
  {
    
    question: "Which of the following tag is used to embed css in html page?",
    option1: "<css>",
    option2: "<!DOCTYPE html>",
    option3: "<script>",
    option4: "<style>",
    ans: 4
  },
  {
    question: "Which of the following has introduced text, list, box, margin, border, color, and background properties?",
    option1: "HTML",
    option2: "PHP",
    option3: "CSS",
    option4: "Ajax",
    ans: 3
  },
  {
    
    question: "Which of the following CSS framework is used to create a responsive design?",
    option1: "django",
    option2: "rails",
    option3: "larawell",
    option4: "bootstrap",
    ans: 4
  },
  {
    
    question: "Which of the following type of HTML tag is used to define an internal style sheet?",
    option1: "<script>",
    option2: "<link>",
    option3: "<class>",
    option4: "<style>",
    ans: 4
  },
  {
    
    question: "Which of the following CSS property is used to make the text bold?",
    option1: "text-decoration: bold",
    option2: "font-weight: bold",
    option3: "font-style: bold",
    option4: "text-align: bold",
    ans: 2
  },
  {
    
    question: "Which of the following CSS style property is used to specify an italic text?",
    option1: "style",
    option2: "font",
    option3: "font-style",
    option4: "@font-face",
    ans: 3
  },
  {
    
    question: "Which of the following is the correct syntax to link an external style sheet in the HTML file?",
    option1: "<link rel=”stylesheet” href=”style.css” />",
    option2: "<link rel=”stylesheet” src=”style.css” />",
    option3: "<style rel=”stylesheet” src=”style.css” />",
    option4: "<style rel=”stylesheet” link=”style.css” />",
    ans: 1
  },
  {
    
    question: "Which of the following CSS property defines the different properties of all four sides of an element’s border in a single declaration?",
    option1: "border-collapse",
    option2: "border-width",
    option3: "padding",
    option4: "border",
    ans: 2
  },
  {
    
    question: "Which of the following is the correct way to apply CSS Styles?",
    option1: "in an external CSS file",
    option2: "inside an HTML element",
    option3: "inside the <head> section of an HTML page",
    option4: "all of the mentioned",
    ans: 4
  },
  {
    
    question: "Which of the following CSS property sets the font size of text?",
    option1: "font-size",
    option2: "text-size",
    option3: "text",
    option4: "size",
    ans: 1
  },
  {
    
    question: "Which of the following is not the property of the CSS box model?",
    option1: "margin",
    option2: "color",
    option3: "width",
    option4: "height",
    ans: 2
  },
  {
    
    question: "Which of the following CSS property sets what kind of line decorations are added to an element, such as underlines, overlines, etc?",
    option1: "text-decoration",
    option2: "text-style",
    option3: "text-decoration-line",
    option4: "text-line",
    ans: 3
  },
  {
    
    question: "Which of the following CSS property sets the shadow for a box element?",
    option1: "set-shadow",
    option2: "box-shadow",
    option3: "shadow",
    option4: "canvas-shadow",
    ans: 2
  },
  {
    
    question: "Which of the following CSS property is used to set the color of the text?",
    option1: "text-decoration",
    option2: "pallet",
    option3: "colour",
    option4: "color",
    ans: 4
  },
  {
   
    question: "Which of the following CSS Property controls how an element is positioned?",
    option1: "static",
    option2: "position",
    option3: "fix",
    option4: "set",
    ans: 2
  },
  {
   
    question: "Which of the following CSS property is used to specify table borders in CSS?",
    option1: "table:border",
    option2: "table",
    option3: "border",
    option4: "none of the mentioned",
    ans: 3
  },
  {
    
    question: "Which of the following property is used to align the text in a table?",
    option1: "text-align",
    option2: "align",
    option3: "text",
    option4: "none of the mentioned",
    ans: 1
  },
  {
    
    question: "Which of the following CSS Property sets the stacking order of positioned elements?",
    option1: "y-index",
    option2: "z-index",
    option3: "x-index",
    option4: "all of the mentioned",
    ans: 2
  },
  {
    
    question: "Which of the following CSS property defines the space between cells in a table?",
    option1: "border-spacing",
    option2: "border-style",
    option3: "border",
    option4: "none of the mentioned",
    ans: 1
  },
  {
    
    question: "In CSS, h1 can be called as _______",
    option1: "Selector",
    option2: "Attribute",
    option3: "Value",
    option4: "Tag",
    ans: 1
  },
  {
   
    question: "In css, “color:red” can be called as _____________",
    option1: "Selector",
    option2: "Rule",
    option3: "Property",
    option4: "Property-Name",
    ans: 4
  },
  {
    
    question: "_________ selectors are used to specify a group of elements.",
    option1: "id",
    option2: "class",
    option3: "tag",
    option4: "both class and tag",
    ans: 2
  },
  {
    
    question: "Which of the following property is used to control the space between the border and content in a table?",
    option1: "border",
    option2: "margin",
    option3: "padding",
    option4: "resize",
    ans: 3
  },{
  
    question: "__________ has introduced text, list, box, margin, border, color, and background properties.",
    option1: "css",
    option2: "html",
    option3: "ajax",
    option4: "php",
    ans: 1
  },
  {
    
    question: "Which of the following is not a value for font-style property?",
    option1: "normal",
    option2: "italic",
    option3: "oblique",
    option4: "none of the above",
    ans: 4
  },
  {
    
    question: "Which CSS property is equivalent for the attribute <center>?",
    option1: "color",
    option2: "margin",
    option3: "align",
    option4: "font",
    ans: 3
  },
  {
   
    question: "Which CSS property is Equivalent for the align attribute?",
    option1: "float",
    option2: "text-align",
    option3: "centre",
    option4: "float & text-align",
    ans: 1
  },
  {
    
    question: "Which of the following property sets the font size of text?",
    option1: "text-size",
    option2: "font-size",
    option3: "text",
    option4: "size",
    ans: 2
  },
  {
   
    question: "Which of the following property sets the amount of spacing between letters?",
    option1: "space",
    option2: "line-height",
    option3: "letter-spacing",
    option4: "letter-space",
    ans: 3
  }
]