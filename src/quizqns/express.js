export const data =
[{
    question: "What is Express.js?",
    option1: "A database management system",
    option2: "A front-end framework",
    option3: "A back-end framework for Node.js",
    option4: "A JavaScript library for animations",
    ans: 3
  },
  {
    question: "Which command is used to install Express.js using npm?",
    option1: "npm install express",
    option2: "npm express install",
    option3: "install express",
    option4: "express install",
    ans: 1
  },
  {
    question: "What is the purpose of the Express.js middleware?",
    option1: "To manage database connections",
    option2: "To handle HTTP requests and responses",
    option3: "To style HTML pages",
    option4: "To create animations",
    ans: 2
  },
  {
    question: "How can you handle route parameters in Express.js?",
    option1: "Using the .params method",
    option2: "Using the .routeParams method",
    option3: "Using the .query method",
    option4: "Using the .params property",
    ans: 4
  },
  {
    question: "What does the res.json() function do in Express.js?",
    option1: "Sends a JSON response to the client",
    option2: "Redirects the client to another route",
    option3: "Renders an HTML page",
    option4: "Sends a plain text response",
    ans: 1
  },
  {
    question: "In Express.js, what does the app.use() function do?",
    option1: "Defines a new route",
    option2: "Installs middleware",
    option3: "Sets the server port",
    option4: "Initializes the Express application",
    ans: 2
  },
  {
    question: "Which template engine is commonly used with Express.js for server-side rendering?",
    option1: "Pug (formerly Jade)",
    option2: "EJS (Embedded JavaScript)",
    option3: "Handlebars",
    option4: "All of the above",
    ans: 4
  },
  {
    question: "How do you handle form submissions in Express.js?",
    option1: "Using the .form() method",
    option2: "Using the .body property",
    option3: "Using the .formParser() middleware",
    option4: "Using the .bodyParser() middleware",
    ans: 4
  },
  {
    question: "What is the purpose of the Express.js router?",
    option1: "To define middleware functions",
    option2: "To create route handlers",
    option3: "To manage database connections",
    option4: "To handle HTTP responses",
    ans: 2
  },
  {
    question: "How can you serve static files in Express.js?",
    option1: "Using the express.static() middleware",
    option2: "Using the .serveStatic() method",
    option3: "Using the staticFiles() function",
    option4: "Using the .staticFiles() method",
    ans: 1
  },
  {
    question: "Which method is used to handle HTTP DELETE requests in Express.js?",
    option1: ".delete()",
    option2: ".del()",
    option3: ".remove()",
    option4: ".destroy()",
    ans: 2
  },
  {
    question: "What is the purpose of the express.Router() function in Express.js?",
    option1: "To create a new Express application",
    option2: "To define middleware functions",
    option3: "To create modular route handlers",
    option4: "To configure the server port",
    ans: 3
  },
  {
    question: "How do you set up a basic Express.js server to listen on a specific port?",
    option1: "express.listen(3000)",
    option2: "express.app(3000)",
    option3: "app.listen(3000)",
    option4: "app.start(3000)",
    ans: 3
  },
  {
    question: "What is the purpose of the express.json() middleware in Express.js?",
    option1: "To parse JSON-encoded request bodies",
    option2: "To send JSON responses to the client",
    option3: "To set up JSON web tokens",
    option4: "To configure JSON formatting options",
    ans: 1
  },
  {
    question: "How can you handle errors in Express.js?",
    option1: "Using the .errorHandler() middleware",
    option2: "Using the .catchErrors() method",
    option3: "Using the .onError() method",
    option4: "Using middleware with four parameters (err, req, res, next)",
    ans: 4
  },
  {
    question: "What is the purpose of the express.urlencoded() middleware in Express.js?",
    option1: "To parse URL-encoded request bodies",
    option2: "To encode URL parameters",
    option3: "To handle URL routing",
    option4: "To serve static files",
    ans: 1
  },
  {
    question: "How do you install the nodemon package for automatic server restarts during development in Express.js?",
    option1: "npm install nodemon",
    option2: "nodemon install express",
    option3: "npm express install --save-dev nodemon",
    option4: "npm install express --global nodemon",
    ans: 1
  },
  {
    question: "Which method is used to handle HTTP PUT requests in Express.js?",
    option1: ".put()",
    option2: ".update()",
    option3: ".modify()",
    option4: ".change()",
    ans: 1
  },
  {
    question: "What is the purpose of the express.static() middleware in Express.js?",
    option1: "To handle static file requests",
    option2: "To generate dynamic content",
    option3: "To parse JSON-encoded requests",
    option4: "To handle authentication",
    ans: 1
  },
  {
    question: "How can you access URL parameters in Express.js?",
    option1: "Using the .params property",
    option2: "Using the .body property",
    option3: "Using the .query property",
    option4: "Using the .urlParams property",
    ans: 1
  },
  {
    question: "What does the res.send() function do in Express.js?",
    option1: "Sends a JSON response to the client",
    option2: "Sends a plain text response to the client",
    option3: "Renders an HTML page",
    option4: "Redirects the client to another route",
    ans: 2
  },
  {
    question: "How can you set up route middleware in Express.js?",
    option1: "Using the .use() method",
    option2: "Using the .middleware() method",
    option3: "Using the .routeMiddleware() function",
    option4: "Using the .applyMiddleware() method",
    ans: 1
  },
  {
    question: "What is the purpose of the express.Router() function in Express.js?",
    option1: "To define middleware functions",
    option2: "To create a new Express application",
    option3: "To create modular route handlers",
    option4: "To configure the server port",
    ans: 3
  },
  {
    question: "How do you handle file uploads in Express.js?",
    option1: "Using the .upload() method",
    option2: "Using the .handleUpload() method",
    option3: "Using the .multer() middleware",
    option4: "Using the .fileHandler() method",
    ans: 3
  },
  {
    question: "Which method is used to handle HTTP PATCH requests in Express.js?",
    option1: ".patch()",
    option2: ".update()",
    option3: ".modify()",
    option4: ".change()",
    ans: 1
  },
  {
    question: "What is the purpose of the express.cookieParser() middleware in Express.js?",
    option1: "To parse cookie headers",
    option2: "To create cookies",
    option3: "To handle cookie-based authentication",
    option4: "To encrypt cookies",
    ans: 1
  },
  {
    question: "How can you set up session handling in Express.js?",
    option1: "Using the .session() middleware",
    option2: " Using the .createSession() method",
    option3: "Using the .expressSession() middleware",
    option4: "Using the .handleSession() method",
    ans: 3
  },
  {
    question: "What is the purpose of the express.json() middleware in Express.js?",
    option1: "To parse JSON-encoded request bodies",
    option2: "To send JSON responses to the client",
    option3: "To set up JSON web tokens",
    option4: "To configure JSON formatting options",
    ans: 1
  },
  {
    question: "How do you redirect a client to another route in Express.js?",
    option1: "Using the .redirect() method",
    option2: "Using the .sendRedirect() method",
    option3: "Using the .res.redirect() method",
    option4: "Using the .routeTo() method",
    ans: 3
  },
  {
    question: "What is the purpose of the express.urlencoded() middleware in Express.js?",
    option1: "To parse URL-encoded request bodies",
    option2: "To encode URL parameter",
    option3: "To handle URL routing",
    option4: "To serve static files",
    ans: 1
  }]